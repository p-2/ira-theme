<?php
/**
 * IRA theme functions
 */

/* post types */
include 'lib/class-ira-post-types.php';

/* set up theme admin options and run upgrades */
include 'lib/class-ira-admin.php';

/* bootstrapping */
include 'lib/class-ira-setup.php';

/* search */
include 'lib/class-ira-search.php';

/* media */
include 'lib/class-ira-media.php';

/* navigation */
include 'lib/class-ira-navigation.php';

/* scripts */
include 'lib/class-ira-scripts.php';

/* utilities */
include 'lib/class-ira-hooks.php';

/* shortcodes */
include 'lib/class-ira-shortcodes.php';

/* ACF stuff */
include 'lib/class-ira-acf.php';