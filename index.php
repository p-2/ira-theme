<?php

get_header();
if ( is_single() || is_page() ) {
    get_template_part( 'templates/single', get_post_type() );
} else {
    get_template_part( 'templates/archive', get_post_type() );
}
get_footer();