Ian Ritchie Architects Wordpress Theme
======================================

Overview
--------

This repository contains the new wordpress theme for http://ianritchiearchitects.co.uk

The current website is driven by Wordpress, but utilises a number of custom plugins to create post types, add content fields, and provide integrations for google maps, a custom search, and other features.

The main purpose of the development of this new theme is to apply a new design to the website. It is also to remove the dependencies on all custom plugins, and to port and convert the data held in all custom fields to Advanced Custom Fields Pro.

Design
------

Images of the layouts, desgn notes and the designer's sketch source file are available [in Dropbox](https://www.dropbox.com/sh/701iygaij29ojp7/AADmTLWl8eQJMvr75N3CSMqka?dl=0).

Data
----

A dump of the uploads directory of the current site (with all thumbnails removed) and a dump of the database are available [in Dropbox](https://www.dropbox.com/sh/u053eulry37swmy/AAD8lBswkdiBcZJENtb3JC6Ia?dl=0)

Schedule
--------

|Process                   |Dates                 |
|--------------------------|----------------------|
|Preparation and audit     |31st May - 6th June   |
|Realise designs in HTML   |6th - 13th June       |
|HTML review and sign off  |13th - 20th June      |
|Wordpress build           |20th June - 4th July  |
|Content import/adaptation |4th - 11th July       |
|Testing                   |11th - 18th July      |
|Launch                    |13th Sept             |


ACF field groups and Custom Post Types
--------------------------------------

###Project custom fields

 * Slideshow
 * Aditional Media (uploaded files)
 * Related Content (Posts)
 * Project details (year and project number)
 * Project Data (Awards, Bibliography, External Links)
 * Project map

###Innovation custom fields

 * Slideshow
 * Related Content (Posts)

###Product custom fields

 * Slideshow
 * Related Content (Posts)

###Literature custom fields

 * Aditional Media (uploaded files)
 * Related Content (Posts)

