<?php

get_header();

print('<div class="container main">');

// check if the flexible content field has rows of data
if( have_rows('block') ):

    // loop through the rows of data
    while ( have_rows('block') ) : the_row();

		switch ( get_row_layout() ) {
			case "featured":
			case "slider":
				get_template_part('templates/home-blocks/slider');
				break;
			case "text_carousel":
				get_template_part('templates/home-blocks/text-carousel');
				break;
			default:
			    get_template_part('templates/home-blocks/static');
			    break;
		}


    endwhile;

else :

    // no layouts found

endif;
print('</div>');

get_footer();
