// Gulp
var gulp = require('gulp');

// Sass/CSS stuff
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

// JavaScript
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

// Images
var svgmin = require('gulp-svgmin');
var imagemin = require('gulp-imagemin');

// Utilities
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var header = require('gulp-header');
var gutil = require('gulp-util');
var fs = require('fs');
var exec = require('child_process').execSync;

// read package.json
var pkg = JSON.parse(fs.readFileSync('./package.json'));

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/src/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// copy vendor stuff
gulp.task('copy-vendor', function(){
	gulp.src('bower_components/featherlight/src/featherlight.css')
	.pipe(rename('_featherlight.dist.scss'))
	.pipe(gulp.dest('scss/components'));
	gulp.src('bower_components/featherlight/src/featherlight.gallery.css')
	.pipe(rename('_featherlight.gallery.dist.scss'))
	.pipe(gulp.dest('scss/components'));
	gulp.src('bower_components/jquery/dist/jquery.min.js')
	.pipe(gulp.dest('js/vendor'));

});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/style.scss')
        .pipe(sass({
            includePaths: ['./scss'],
            outputStyle: 'expanded'
        }))
        .on('error', gutil.log)
        .pipe(prefix(
            "last 3 version", "> 1%", "ie >= 9"
        ))
        .on('error', gutil.log)
        .pipe(gulp.dest('css'))
        .pipe(rename('style.min.css'))
        .pipe(cleanCSS())
        .on('error', gutil.log)
        .pipe(gulp.dest('css'));
});

// Concatenate & Minify JS
gulp.task('scripts', ['lint'], function() {
    return gulp.src([
    		'bower_components/jquery-detect-swipe/jquery.detect_swipe.js',
    		'bower_components/featherlight/src/featherlight.js',
    		'bower_components/featherlight/src/featherlight.gallery.js',
    		'bower_components/jquery.fn/sortElements/jquery.sortElements.js',
	   		'bower_components/matchHeight/jquery.matchHeight.js',
	   		'bower_components/bxslider-4/dist/jquery.bxslider.js',
    		'js/src/*.js'
    	])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest('js'))
        .pipe(rename('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});
// get vendor scripts
gulp.task('mapscripts', function() {
    return gulp.src(['bower_components/js-marker-clusterer/src/markerclusterer.js'])
        .pipe(uglify())
        .pipe(gulp.dest('js/vendor'))
});


gulp.task('themeheader', ['bump'], function() {
	var pkg = JSON.parse(fs.readFileSync('./package.json'));
	// banner for style.css
	var cssbanner = ['/*',
	  'Theme Name:          '+pkg.themename,
	  'Description:         '+pkg.description,
	  'Author:              '+pkg.author,
	  'Version:             '+pkg.version,
	  'Theme URI:           '+pkg.homepage,
	  'Bitbucket Theme URI: '+pkg.homepage,
	  'License:             '+pkg.licensename,
	  'License URI:         '+pkg.licenseuri,
	  '*/',
	  ''].join('\n');
	fs.writeFile('style.css', cssbanner);
});

// Images
gulp.task('svgmin', function() {
    gulp.src('img/src/*.svg')
        .pipe(svgmin())
        .pipe(gulp.dest('img'));
});

gulp.task('imagemin', function () {
    gulp.src('img/src/*')
        .pipe(imagemin())
        .pipe(gulp.dest('img'));
});

// Bump version
gulp.task('bump', function(){
	var pkg = JSON.parse(fs.readFileSync('./package.json'));
	return gulp.src(['lib/admin.php'])
		.pipe(replace(/version = "[0-9\.]*";/g, 'version = "'+pkg.version+'";'))
		.pipe(gulp.dest('lib'))
});

// tag and commit
gulp.task('tagcommit', ['bump','themeheader'], function(){
	var pkg = JSON.parse(fs.readFileSync('./package.json'));
	exec('git commit -a -m "tagging version '+pkg.version+'"');
	exec('git tag '+pkg.version);
	exec('git push --tags');
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('js/src/*.js', ['lint', 'scripts', 'mapscripts']);
    gulp.watch('scss/**/*.scss', ['sass']);
    gulp.watch('package.json', ['bump', 'themeheader'])
});

// Default Task
gulp.task('tag', ['bump', 'themeheader', 'tagcommit']);
gulp.task('default', ['lint', 'copy-vendor', 'sass', 'themeheader', 'scripts', 'watch']);
