/**
 * Clone the section titles to allow text-decoration transition
 * REF URL: http://codepen.io/standardspace/pen/dXOJNq
 */


$(".homepage-slider--item--title").each(function(){
    $(this).clone().addClass('cloned').prependTo($(this).parent());
});