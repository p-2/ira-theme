// bxslider
(function($){
	var respMax = 768,
	sliders = [],
	win_resize = null,
	resize_timeout = 500,
	slider_opts = {
		adaptiveHeight:true,
		nextText:'',
		prevText:'',
		touchEnabled:false
	},
	handleResize = function()
	{
   		if (getViewportWidth() >= respMax) {
			activateSliders();
		} else {
			cancelSliders();
		}
	},
	activateSliders = function(){
		if ( ! sliders.length && $('.JS-slider').length ) {
			$('.JS-slider').each(function(){
				sliders.push($(this).bxSlider(slider_opts));
			});
		}
	},
	cancelSliders = function(){
		if ( sliders.length ) {
			$.each(sliders, function(){
				this.destroySlider();
			});
			sliders = [];
		}
	},
	getViewportWidth = function(){
		document.body.style.overflow = "hidden";
		var viewportWidth = $(window).width();
		document.body.style.overflow = "";
		return viewportWidth;
	};
   	if (getViewportWidth() >= respMax) {
		activateSliders();
	}
	// trigger handler when window is resized
	$(window).resize(function() {
		clearTimeout(win_resize);
		win_resize = setTimeout(function(){
			handleResize();
		}, resize_timeout);
	});
    $('.JS-home-text-carousel').bxSlider({
        controls: false,
        auto:true,
        infiniteLoop:true,
        pause:8000
    });
})(jQuery);
