(function($){
	var awards = [],
	getAward = function(aid)
	{
		for (var a = 0; a < awards.length; a++){
			if (awards[a].id == aid) {
				return '<dd>'+awards[a].el+'</dd>';
			}
		}
	};
	if ($('.awards').length && typeof ad !== 'undefined') {
		$('.awards').before('<div class="awardsnav"><dl><dt>Sort: </dt><dd class="active"><a href="#" class="sortawards" data-sortby="year" title="Sort awards by date">Date</a></dd><dd><a href="#" class="sortawards" data-sortby="awarding_body" title="Sort awards by awarding body">Awarding Body</a></dd><dd><a href="#" class="sortawards" data-sortby="award_type" title="Sort Awards by type">Type</a></dd><dd><a href="#" class="sortawards" data-sortby="project_title" title="Sort awards by project">Project</a></dd></dl></div>');
		$('.a-bydate').hide();
		$('.awards dd').each(function(idx){
			awards[awards.length] = {
				'id':$(this).data("awardid"),
				'el':$(this).html()
			};
		});
		$('.sortawards').on('click',function(e){
			e.preventDefault();
			if ( ! $(this).parent('dd').hasClass("active") ) {
				$('.awardsnav dd').removeClass("active");
				$(this).parent('dd').addClass("active");
				$('.awards').empty();
				if (ad[$(this).data("sortby")]) {
					var list = $('<dl/>'),
						data = ad[$(this).data("sortby")],
						keys = Object.keys(data);
					if ($(this).data("sortby") === 'year') {
						keys.sort().reverse();
					} else {
						keys.sort(function(a,b){if(a.toUpperCase()==b.toUpperCase()){return 0;}return (a.toUpperCase()<b.toUpperCase())?-1:1;});
					}
					for (var i = 0; i < keys.length; i++) {
						if ($(this).data("sortby") === 'project_title') {
							var title = keys[i].split('|');
							list.append('<dt><a href="'+title[1]+'">'+title[0]+'</a></dt>');
						} else {
							list.append('<dt>'+keys[i]+'</dt>');
						}
						for (var j = 0; j < data[keys[i]].length; j++) {
							list.append(getAward(data[keys[i]][j]));
						}
					}
					$('.awards').append(list);
				}
				$('.awards dd span').show();
				$('.hide-'+$(this).data("sortby")).hide();
				//$('html, body').animate({scrollTop: 0}, 1000);
			}
		});
	}

})(jQuery);