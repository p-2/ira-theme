/*
Navigation Toggle
*/
(function($) {

    "use strict";

    // Mobile Navigation toggling

    $(".JS-navigation-toggle").on('click', function(){
        $(this).toggleClass('is-active');
        $('.JS-nav-main').toggleClass('is-active');
        $('body').toggleClass('disable-scroll');
    });

    $(".JS-subnav-toggle").on('click', function(){
    	$(this).toggleClass("is-active");
    	$(".JS-subnav").toggleClass("is-active");
    });

	// add class to Header on scroll

    var header = $('.JS-site-header');

/*    $( document ).scroll(function(e) {
        if (document.body.scrollTop > 80 ||  document.documentElement.scrollTop > 80) {
            header.addClass("scrolled");
        } else {
            header.removeClass("scrolled");
        }
    }); */

})(jQuery);