/** 
 * modals
 */
(function($) {
	// Search modal

    $('.JS-search-open').on('click', function(e) {
        e.preventDefault();
        $('.JS-search-container').addClass('is-active');
        $('body').addClass('disable-scroll');
        $('.JS-nav-main').removeClass('is-active');
        setTimeout(function(){
        	$('#search-modal-input').focus();
        },500);
    });

    $('.JS-search-close').on('click', function() {
        $('.JS-search-container').removeClass('is-active');
        $('.JS-navigation-toggle').removeClass('is-active');
        window.focus();
        $('body').removeClass('disable-scroll');
    });

    /* featherlight gallery */

    $('.gallery-lightbox').featherlightGallery({
        gallery: {
            fadeIn: 300,
            fadeOut: 300
        },
        openSpeed:    300,
        closeSpeed:   300
    });

    /* modals */
    if ( $('.modal').length ) {
    	$('.modal').each(function(){
    		if ($(this).hasClass('modal-video')) {
    			$(this).featherlight({
    				openSpeed:      300,
			        closeSpeed:     300,
			        iframe:         $(this).attr('href'),
			        afterContent:   function(){
			        	this.$content.attr({'allowFullScreen':'','webkitAllowFullScreen':'','mozAllowFullScreen':'','style':'border:0'});
			        }
    			});
    		} else if ($(this).hasClass('modal-audio')) {
    			$(this).on('click', function(e){
    				e.preventDefault();
    				$.featherlight('<audio src="'+$(this).attr('href')+'" style="width:100%" controls autoplay><a href="'+$(this).attr('href')+'">'+$(this).text()+'</a></audio>');
    			});
    		} else {
			    $(this).featherlight({
			        openSpeed:      300,
			        closeSpeed:     300
			    });
    		}
    	});
    }



})(jQuery);