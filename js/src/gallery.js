(function($){
    $.featherlight.defaults.background = [
                        '<div class="featherlight-loading featherlight">',
                        '<span class="featherlight-close-icon featherlight-close">',
                        '<span></span>',
                        '</span>',
                        '<div class="featherlight-content">',
                        '<div class="featherlight-inner"></div>',
                        '</div>',
                        '</div>'].join('');
    $.featherlightGallery.defaults.previousIcon = '';
    $.featherlightGallery.defaults.nextIcon = '';
    $.featherlightGallery.prototype.afterContent = function() {
        var imgObj = this.$currentTarget.find('img');

        var imgTitle = $(imgObj).data('title'),
            imgCaption = $(imgObj).data('caption'),
            imgDesc = $(imgObj).data('description');

        // Check var values and build string
        var text = "";
        if (imgTitle) {
            text += '<h3 class="image-caption--title">' + imgTitle + '</h3>';
        }
        if (imgDesc) {
            text += '<p class="description">' + imgDesc + '</p>';
        }
        if (imgCaption) {
            text += imgCaption;
        }

        // Delete the previous caption if one exists
        this.$instance.find('.image-caption').remove();

        // Create the new caption
        if ( imgTitle || imgCaption || imgDesc ) {
            $('<div class="image-caption">').html(text).appendTo(this.$instance.find('.featherlight-content'));
        }


       resizeCaption();

    };

    // Resize Caption to match image width
    var resizeCaption = function() {
        var imageWidth = $('.featherlight-content img').width();

        $('.featherlight .image-caption').width(imageWidth);
    };

    window.onresize = function(event) {
        resizeCaption();
    };


})(jQuery);