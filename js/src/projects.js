(function($){
	var page_title = $('h1.page-title').text(),
	slideQueue =  $({}),
	sortState = '',
	// refreshes the projects page after sort/filter
	refreshProjects = function( filter )
	{
		if ( false === filter ) {
			$('.list-item').matchHeight().fadeIn('fast');
		} else {
			$('.list-item').hide();
			$('.type-'+filter).matchHeight().fadeIn('fast');
		}

		/* go to the top of the screen */
		if ($('html').prop('scrollTop') > 0) {
			$('html').animate({scrollTop: 0}, 500);
		}
	};
	// links to sort and filter projects
	$('.sortby-atoz').click(function(e){
		e.preventDefault();
		sortState = '#az';
		if ($(this).parent().hasClass('desc')) {
			$(this).parent().addClass('asc').removeClass('desc');
			sortState += '_asc';
			$('.list-item:visible').sortElements(function(a, b){
				return $(a).attr('data-sortkey_atoz') < $(b).attr('data-sortkey_atoz') ? 1 : -1;
			});
		} else {
			$(this).parent().addClass('desc').removeClass('asc');
			sortState += '_desc';
			$('.list-item:visible').sortElements(function(a, b){
				return $(a).attr('data-sortkey_atoz') > $(b).attr('data-sortkey_atoz') ? 1 : -1;
			});
		}
		$('.list-item:visible').matchHeight();
		$('.sortby-date').parent().removeClass('active asc desc');
		$(this).parent().addClass('active');
		
	});
	$('.sortby-date').click(function(e){
		e.preventDefault();
		sortState = '#date';
		if ($(this).parent().hasClass('desc')) {
			sortState += '_asc';
			$(this).parent().addClass('asc').removeClass('desc');
			$('.list-item:visible').sortElements(function(a, b){
				return $(a).attr('data-sortkey_date') > $(b).attr('data-sortkey_date') ? 1 : -1;
			});
		} else {
			$(this).parent().addClass('desc').removeClass('asc');
			sortState += '_desc';
			$('.list-item:visible').sortElements(function(a, b){
				return $(a).attr('data-sortkey_date') < $(b).attr('data-sortkey_date') ? 1 : -1;
			});
		}
		$('.list-item:visible').matchHeight();
		$('.sortby-atoz').parent().removeClass('active asc desc');
		$(this).parent().addClass('active');

	});

	$('.filter').on('click', function(e){
		// only filter main project archive
		if ( $('body').hasClass('post-type-archive-project') ) {
			e.preventDefault();

			// get project type slug
			var type = $(this).attr('data-type-slug');

			// save URL
			var currentURL;

			// see what is active
			if ( $(this).parent().hasClass('active') ) {

				// cancels active filter and displays all
				$('.filters dd').removeClass('active');
				$('.show-all').addClass( 'active' );
				window.history.pushState( {'type':'all','title':page_title,'sort':sortState}, '', $('.show-all').attr('href') );
				$('h1.page-title').text(page_title);
				refreshProjects( false );

			} else {

				// adds new filter and displays subset
				$('.filters dd').removeClass('active');
				$(this).parent().addClass('active');
				$('h1.page-title').text($(this).text());
				window.history.pushState({'type':type,'title':$(this).text(),'sort':sortState}, '', $(this).attr('href') );
				refreshProjects( type );
			}
			
		}
	});
	$('.show-all').on('click', function(e){
		// only filter main project archive
		if ( $('body').hasClass('post-type-archive-project') ) {
			e.preventDefault();
			if ( ! $(this).parent().hasClass('active') ) {
				// show all projects
				$('.filters dd').removeClass('active');
				$(this).parent().addClass('active');
				$('h1.page-title').text(page_title);
				window.history.pushState( {'type':'all','title':page_title}, '', $(this).attr('href') );
				refreshProjects( false );
			}
		}
	});
	$('.change-view').on('click', function(e){
		e.preventDefault();
		if ( ! $(this).parent().hasClass('active') ) {
			$('.subnav-list.layout dd').removeClass('active');
			$(this).parent().addClass('active');
			if ($(this).hasClass('togrid')) {
				$('.posts-list').removeClass('list').addClass('grid');
			} else {
				$('.posts-list').removeClass('grid').addClass('list');
			}
			$('.list-item').matchHeight();
		}
	});

	// handle popstates
	window.onpopstate = function(e){
		if ( ! e.state || e.state.type == 'all') {
			$('h1.page-title').text(page_title);
			$('.filters dd').removeClass('active');
			$('.show-all').parent().addClass('active');
			refreshProjects( false );
		} else {
			$('h1.page-title').text(e.state.title);
			$('.filters dd').removeClass('active');
			$('[data-type-slug='+e.state.type+']').parent().addClass('active');
			refreshProjects( e.state.type );
		}
	};

	// handle load
	window.onload = function(){
		//$('.sortby-atoz, .sortby-date').parent().removeClass('active asc desc');
		switch(window.location.hash){
			case '#az_asc':
				$('.sortby-atoz').parent().addClass('asc').addClass('active');
				$('.list-item:visible').sortElements(function(a, b){
					return $(a).attr('data-sortkey_atoz') < $(b).attr('data-sortkey_atoz') ? 1 : -1;
				});
				break;
			case '#az_desc':
				$('.sortby-atoz').parent().addClass('desc').addClass('active');
				$('.list-item:visible').sortElements(function(a, b){
					return $(a).attr('data-sortkey_atoz') > $(b).attr('data-sortkey_atoz') ? 1 : -1;
				});
				break;
			case '#date_asc':
				$('.sortby-date').parent().addClass('asc').addClass('active');
				$('.list-item:visible').sortElements(function(a, b){
					return $(a).attr('data-sortkey_date') > $(b).attr('data-sortkey_date') ? 1 : -1;
				});
				break;
			case '#date_desc':
				$('.sortby-date').parent().addClass('desc').addClass('active');
				$('.list-item:visible').sortElements(function(a, b){
					return $(a).attr('data-sortkey_date') < $(b).attr('data-sortkey_date') ? 1 : -1;
				});
				break;
		}
		$('.list-item:visible').matchHeight();
	};
})(jQuery);