var mapApp = (function($) {

    'use strict';

    var currentMap = false,
        bodyStyle = ((document.body.runtimeStyle) ? document.body.runtimeStyle : document.body.style),
        originalOverflow = bodyStyle.overflow,

        // Map initialisation
        // called for the first time by a google maps API callback
        // thereafter called by openMap (via includeAPI)
        mapInit = function() {
        	// if the map hasn't been initialised yet, make the map
            if (!$(currentMap).data('map')) {
            	var startType = 'SATELLITE';
            	if ($(currentMap).attr('data-start-type')) {
            		if (-1 !== $.inArray($(currentMap).data('start-type').toUpperCase(), ['ROADMAP','SATELLITE','HYBRID','TERRAIN'])) {
            			startType = $(currentMap).data('start-type').toUpperCase();
            		}
            	}
            	var startZoom = 16;
            	if ($(currentMap).attr('data-start-zoom')) {
            		if (parseInt($(currentMap).attr('data-start-zoom')) > 0 && parseInt($(currentMap).attr('data-start-zoom')) < 21) {
            			startZoom = parseInt($(currentMap).attr('data-start-zoom'));
            		}
            	}
                var mapOptions = {
                    center: new google.maps.LatLng(51.508352, -0.029845),
                    zoom: startZoom,
                    mapTypeId: google.maps.MapTypeId[startType],
                    scrollwheel: false
                };
                $(currentMap).data('map', new google.maps.Map(document.getElementById($(currentMap).find('.map:first').attr('id')), mapOptions));
                $(currentMap).data('markers', []);

                // add markers
                $(currentMap).find('.marker').each(function() {
                    addMarker({
                		'lat':$(this).data('lat'),
                		'lng':$(this).data('lng'),
                		'title':$(this).data('title'),
                		'html':$(this).html()
                	}, $(currentMap).data('map'));
                });
            }
            // now show the map
            $(currentMap).fadeIn();
            document.body.style.overflow = "hidden";
            // activate close button
            $(currentMap).find('.close-map-button').show().on('click', closeMap);
            // redraw
            google.maps.event.trigger($(currentMap).data('map'), 'resize');
            // centre
            centreMap($(currentMap).data('map'));
            // callback?
            if ($(currentMap).data('markers').length === 1) {
            	google.maps.event.trigger($(currentMap).data('markers')[0], 'click', {});
            	/*$.post(
            		iratheme.ajax_url,
            		{'action':$(currentMap).data('callback')},
            		function(response) {
            			google.maps.event.trigger($(currentMap).data('markers')[0], 'click', {});
            			$.each(response, function(){
            				addMarker(this, $(currentMap).data('map'));
            			});
            			$.getScript(iratheme.dir+'/js/vendor/markerclusterer.js', function(){
            				var markerClusterer = new MarkerClusterer($(currentMap).data('map'), $(currentMap).data('markers'), {'imagePath': iratheme.dir+'/img/icons/m'});
            			});
            		},
            		'json'
            	);*/
            }
        },

        // Include Google Maps API
        includeAPI = function() {
            var js,
                fjs = document.getElementsByTagName('script')[0];

            if (!document.getElementById('gmap-api')) {
                js = document.createElement('script');
                js.id = 'gmap-api';
                js.setAttribute('async', '');
                js.src = "//maps.google.com/maps/api/js?key=" + iratheme.google_api_key + "&v=3&callback=mapApp.mapInit";
                fjs.parentNode.insertBefore(js, fjs);
            } else {
            	// API has already been loaded - just run mapInit
                mapInit();
            }
        },

        // Centre map on screen
        centreMap = function(map) {
            // get bounds object
            var bounds = new google.maps.LatLngBounds();

            // loop through all markers and extend bounds
            $.each($(currentMap).data('markers'), function(i, marker) {
                var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                bounds.extend(latlng);
            });
            //  if more than one marker, ignore start_lat and start_lng and fit to bounds
            if ($(currentMap).data('markers').length > 1) {
                map.fitBounds(bounds);
            } else {
                map.setCenter(bounds.getCenter());
            }
        },

        addMarker = function($marker, map) {

            // create marker
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng($marker.lat, $marker.lng),
                icon: {
                    url: iratheme.dir + '/img/icons/map-star.png',
                    size: new google.maps.Size(32, 30),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 16)
                },
                shape: {
                    coords: [0, 11, 11, 11, 16, 0, 21, 11, 32, 11, 23, 19, 26, 30, 16, 23, 6, 30, 9, 19],
                    type: 'poly'
                },
                title: $marker.title,
                map: map
            });

            // add to array
            $(currentMap).data('markers').push(marker);

            // if marker contains HTML, add it to an infoWindow
            if ($marker.html !== '') {
                // create info window
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html
                });

                // show info window when marker is clicked
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            }
        },

        setCurrentMap = function(container_id)
        {
        	if ($(container_id).length === 1) {
                currentMap = container_id;
            } else {
                currentMap = false;
            }
        },

        openMap = function() {
            includeAPI();
        },

        closeMap = function() {
            $(currentMap).find('.close-map-button').hide();
            $(currentMap).fadeOut();
            document.body.style.overflow = originalOverflow;
        };

    $('.show-map').on('click', function(e) {
    	e.preventDefault();
        if ($(this).attr('data-targetid')) {
            setCurrentMap('#' + $(this).attr('data-targetid'));
        } else if ($(this).attr('href')) {
        	setCurrentMap($(this).attr('href'));
        }
        if (currentMap) {
            openMap();
        }
    });



    // Make mapInit() public so we can pass it to
    // the Google Maps API callback param
    return {
        mapInit: mapInit
    };

})(jQuery);
