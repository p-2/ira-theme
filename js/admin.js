(function($){

	/* change Posts to News */
	$('#menu-posts a.menu-top .wp-menu-name').text("News");
	$('#menu-posts a.wp-first-item').text("All News");
	/* remove links to categories and tags for posts */
    $('#menu-posts a').each(function(){
        if ($(this).attr("href").indexOf('edit-tags.php?taxonomy=') != -1) {
            $(this).parent().hide();
        }
    });
	/* hide month dropdown from project pages list */
	if ( window.location.href.indexOf('post_type=project') !== -1 ) {
		if ($('select[name="m"]').length) {
		    $('select[name="m"]').hide();
	    }
	}
})(jQuery);