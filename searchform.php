		<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<label>
				<span class="screen-reader-text">Search for</span>
				<input type="search" class="search-field" id="search-modal-input" placeholder="Search&hellip;" value="<?php echo get_search_query(); ?>" name="s" title="Search for:">
			</label>
			<label class="search-submit">
				<input type="submit" class="search-submit" value="Search" />
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-230 312 18 18" style="enable-background:new -230 312 18 18;" xml:space="preserve"><title>Search icon</title><g class="search"><g transform="translate(-1217.000000, -3.000000)"><g><g><path class="search-outer" d="M1004.6,331l-4.6-4.6c0.8-1.2,1.3-2.6,1.3-4.2c0-4-3.2-7.2-7.2-7.2s-7.2,3.2-7.2,7.2s3.2,7.2,7.2,7.2 c1.6,0,3-0.5,4.2-1.3l4.6,4.6c0.2,0.2,0.5,0.4,0.8,0.4s0.6-0.1,0.8-0.4C1005.1,332.2,1005.1,331.4,1004.6,331z M994.2,327 c-2.7,0-4.8-2.1-4.8-4.8c0-2.7,2.1-4.8,4.8-4.8s4.8,2.1,4.8,4.8C999,324.9,996.9,327,994.2,327z"/></g><circle class="search-inner" cx="994.2" cy="322.2" r="2.8"/></g></g></g></svg>
			</label>
		</form>
