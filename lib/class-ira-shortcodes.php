<?php
/**
 * Shortcodes used in IRA theme
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Shortcodes' ) ) {
	/**
	 * Class to provide a shortcode for the theme
	 */
	class IRA_Shortcodes {
		/**
		 * Constructor - registers all shortcodes
		 */
		public function __construct() {
			/* add shortcode for awards widget */
			add_shortcode( 'awards_widget', array( $this, 'awards_widget_shortcode' ) );

			/* clear transients used by shortcodes */
			add_action( 'save_post', array( $this, 'clear_transients' ), 10, 3 );
		}

		/**
		 * Shortcode for awards_widget
		 * Gets awards from database and returns a list ordered by date, with headings for each year
		 * also generates json object used to dynamically sort the lists
		 *
		 * @param array  $atts - Attributes from shortcode.
		 * @param string $content - content between shortcode tags.
		 */
		public function awards_widget_shortcode( $atts, $content ) {
			$awards = array();
			$awardjson = array(
				'year'          => array(),
				'awarding_body' => array(),
				'project_title' => array(),
				'award_type'    => array(),
			);
			$out = '';
			$awards = get_transient( 'ira_awards' );
			if ( false === $awards ) {
				// Fetch awards from database.
				$projects = get_posts( array(
					'numberposts' => -1,
					'post_type'   => 'project',
					'post_status' => 'publish',
					'nopaging'    => true,
				) );
				$awards = array();
				$award_id = 1;
				foreach ( $projects as $project ) {
					$project_url = get_permalink( $project->ID );
					if ( have_rows( 'awards', $project->ID ) ) {
						while ( have_rows( 'awards', $project->ID ) ) {
							the_row();
							$awarding_body = ( '' === trim( get_sub_field( 'awarding_body' ) ) ) ? 'Others' : get_sub_field( 'awarding_body' );
							$awards[] = array(
								'name'          => get_sub_field( 'name' ),
								'year'          => get_sub_field( 'year' ),
								'awarding_body' => $awarding_body,
								'url'           => get_sub_field( 'url' ),
								'award_type'    => ucfirst( get_sub_field( 'award_type' ) ),
								'project_title' => $project->post_title,
								'project_url'   => $project_url,
								'award_id'      => $award_id,
							);
							$award_id++;
						}
					}
				}
				$this->sort_awards( $awards, array(
					'field' => 'year',
					'dir'   => 'DESC',
					'as'    => 'int',
				) );
				set_transient( 'ira_awards', $awards, DAY_IN_SECONDS );
			}
			if ( count( $awards ) ) {
				$awards_html = array();
				$out .= '<dl class="awards">';
				foreach ( $awards as $award ) {
					// Populate json with id of award.
					foreach ( $awardjson as $key => $data ) {
						$setkey = ( 'project_title' === $key ) ? $award['project_title'] . '|' . $award['project_url'] : $award[ $key ];
						if ( isset( $awardsjson[ $key ][ $setkey ] ) ) {
							$awardsjson[ $key ][ $setkey ][] = $award['award_id'];
						} else {
							$awardsjson[ $key ][ $setkey ] = array( $award['award_id'] );
						}
					}

					if ( ! isset( $awards_html[ $award['year'] ] ) ) {
						$awards_html[ $award['year'] ] = array();
					}
					$awards_html[ $award['year'] ][] = sprintf( '<dd data-awardid="%s"><span class="hide-date">%s</span> <span>%s</span> <span class="hide-project_title"><a href="%s" title="%s">%s</a></span></dd>', $award['award_id'], $award['year'], $award['name'], $award['project_url'], esc_attr( $award['project_title'] ), $award['project_title'] );
				}
				/* reverse sort by year */
				krsort( $awards_html );
				/* gather for output, inserting headings for each year */
				foreach ( $awards_html as $year => $awards_html ) {
					$out .= sprintf( '<dt>%s</dt>%s', $year, implode( '', $awards_html ) );
				}
				$out .= '</dl></div>';

				/* add json to output */
				$out .= '<script type="text/javascript">var ad=' . json_encode( $awardsjson ) . ';</script>';
			}
			return $out;
		}

		/**
		 * Multi-purpose sorting function
		 *
		 * @param array $arr - array to sort (passed by reference).
		 * @param array $args - arguments to determine how to sort.
		 */
		public function sort_awards( &$arr, $args ) {
			$opt = wp_parse_args( $args, array(
				'field' => 'title',
				'dir'   => 'ASC',
				'as'    => 'str',
			) );
			if ( 'str' === $opt['as'] ) {
				usort( $arr, function( $a, $b ) use( $opt ) {
					$cmp = strcasecmp( $a[ $opt['field'] ], $b[ $opt['field'] ] );
					if ( 'ASC' !== $opt['dir'] ) {
						return -$cmp;
					} else {
						return $cmp;
					}
				} );
			}
			if ( 'int' === $opt['as'] ) {
				usort( $arr, function( $a, $b ) use( $opt ) {
					if ( $a[ $opt['field'] ] === $b[ $opt['field'] ] ) {
						return 0;
					}
					if ( 'ASC' === $opt['dir'] ) {
						return ( $a[ $opt['field'] ] < $b[ $opt['field'] ] ) ? -1 : 1;
					} else {
						return ( $a[ $opt['field'] ] > $b[ $opt['field'] ] ) ? -1 : 1;
					}
				} );
			}
		}

		/**
		 * clears transients used by shortcodes
		 *
		 * @param integer $post_id - ID of post being saved.
		 * @param WP_Post $post - post object.
		 * @param boolean $update - whether this is an update or now addition.
		 */
		public function clear_transients( $post_id, $post, $update ) {
			if ( 'project' === $post->post_type ) {
				delete_transient( 'ira_awards' );
				delete_transient( 'ira_maps' );	
			}
		}
	}
	new IRA_Shortcodes();
}