<?php
/**
 * IRA Theme bootstrapping.
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 */
	class IRA_Setup {
		/**
		 * Constructor - activates functions and adds hooks to WordPress API.
		 */
		public function __construct() {
			/* Add style to WordPress editor */
			add_editor_style();

			/* Theme supports woocommerce */
			add_theme_support( 'woocommerce' );

			/* But this isn't being used right now, so disable scripts and styles */
			add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
			add_action( 'init', array( $this, 'remove_woocommerce_scripts' ), 99 );

			/* remove unused links from wp_head() */
			add_action( 'init', array( $this, 'remove_unwanted_links' ) );

			/* disable emojicons */
			add_action( 'init', array( $this, 'disable_wp_emojicons' ) );

			/* remove unused features from pages and posts */
			add_action( 'init', array( $this, 'remove_post_features' ), 0 );

			/* prettify search URLs */
			add_action( 'template_redirect', array( $this, 'change_search_url_rewrite' ) );

			/* add meta tags to head for webmaster tools */
			add_action( 'wp_head', array( $this, 'webmaster_tools_meta' ) );

		}

		/**
		 * Removes unwanted links from head
		 */
		public function remove_unwanted_links() {
			remove_action( 'wp_head', 'rsd_link' );
			remove_action( 'wp_head', 'wlwmanifest_link' );
			remove_action( 'wp_head', 'index_rel_link' );
			remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
			remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
			remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
		}

		/**
		 * Remove woocommerce scripts
		 */
		public function remove_woocommerce_scripts() {
			if ( isset( $GLOBALS['woocommerce'] ) ) {
				remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
				remove_action( 'wp_enqueue_scripts', array( $GLOBALS['woocommerce'], 'frontend_scripts' ) );
			}
		}

		/**
		 * Disable WP emojicons introduced in 4.2
		 * called with the init hook
		 *
		 * @see http://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2
		 */
		public function disable_wp_emojicons() {
			// all actions related to emojis.
			remove_action( 'admin_print_styles', 'print_emoji_styles' );
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );
			remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
			remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
			remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

			// filter to remove TinyMCE emojis.
			add_filter( 'tiny_mce_plugins', array( $this, 'disable_emojicons_tinymce' ) );
		}

		/**
		 * Disable WP emojicons introduced in 4.2
		 * filter for tiny_mce_plugins to disable emoji plugin
		 *
		 * @see http://wordpress.stackexchange.com/questions/185577/disable-emojicons-introduced-with-wp-4-2
		 * @param array $plugins - plugins used in TinyMCE.
		 */
		public function disable_emojicons_tinymce( $plugins ) {
			if ( is_array( $plugins ) ) {
				return array_diff( $plugins, array( 'wpemoji' ) );
			} else {
				return array();
			}
		}

		/**
		 * Removes some features from posts and pages
		 */
		public function remove_post_features() {
			$toremove = array(
				'post' => array( 'trackbacks', 'custom-fields', 'comments', 'page-attributes' ),
				'page' => array( 'trackbacks', 'custom-fields', 'comments' ),
			);
			foreach ( $toremove as $post_type => $features ) {
				foreach ( $features as $feature ) {
					remove_post_type_support( $post_type, $feature );
				}
			}
			add_post_type_support( 'post', 'excerpt' );
			add_post_type_support( 'page', 'excerpt' );
		}

		/**
		 * Make search URLs pretty
		 */
		public function change_search_url_rewrite() {
			if ( is_search() && ! empty( $_GET['s'] ) ) {
				wp_safe_redirect( home_url( '/search/' ) . rawurlencode( get_query_var( 's' ) ) );
				exit();
			}
		}

		/**
		 * Adds meta tags to <head> for webmaster tools
		 */
		public function webmaster_tools_meta() {
			$gwt_key = get_field( 'google_webmaster_tools_key', 'option' );
			if ( $gwt_key ) {
				printf( '<meta name="google-site-verification" value="%s">', $gwt_key );
			}
			$bwt_key = get_field( 'bing_webmaster_tools_key', 'option' );
			if ( $bwt_key ) {
				printf( '<meta name="msvalidate.01" value="%s">', $bwt_key );
			}
		}
	}
	new IRA_Setup();
}
