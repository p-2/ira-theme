<?php
/**
 * Custom post type and taxonomy definitions
 *
 * Projects
 *  |- Project types
 * Literature
 * |- Publication categories
 * Company Profile pages
 * Innovation pages
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Post_Types' ) ) {
	/**
	 * Class used to register custom post types and taxonomies
	 */
	class IRA_Post_Types {
		/**
		 * Constructor - hooks into WordPress API
		 */
		public function __construct() {
			// Register post types and taxonomies.
			add_action( 'init', array( $this, 'register_post_types' ), 10 );
			add_action( 'init', array( $this, 'register_taxonomies' ), 9 );

			// Add a filter dropdown for custom taxonomies.
			add_action( 'restrict_manage_posts', array( $this, 'restrict_by_term' ) );
			add_filter( 'parse_query', array( $this, 'convert_id_to_term_in_query' ) );

			// Add columns to admin table for custom post types.
			add_filter( 'manage_edit-project_columns', array( $this, 'project_columns' ) );
			add_action( 'manage_project_posts_custom_column', array( $this, 'manage_project_columns' ), 10, 2 );
			add_filter( 'manage_edit-project_sortable_columns', array( $this, 'project_sortable_columns' ) );
			add_action( 'pre_get_posts', array( $this, 'projects_admin_orderby' ) );

			// Manage paging in archives.
			add_action( 'pre_get_posts', array( $this, 'adjust_per_page' ) );
			// Put projects in date order.
			add_action( 'pre_get_posts', array( $this, 'order_projects_by_date' ) );
		}

		/**
		 * Adjust pagination on post type archives. Hooked into pre_get_posts.
		 *
		 * @param WP_Query $query - instance of WP_Query passed by reference.
		 */
		public function adjust_per_page( $query ) {
			if ( ! is_admin() && $query->is_main_query() ) {
				/* make sure year based archives display all posts */
				if ( $query->is_archive() && $query->is_year() ) {
					$query->set( 'posts_per_page', -1 );
				}
				/* make sure project pages display all posts */
				if ( $query->is_post_type_archive( 'project' ) || is_tax( 'project_type' ) ) {
					$query->set( 'posts_per_page', -1 );
				}
				/* make sure literature project pages display all posts */
				if ( $query->is_post_type_archive( 'literature' ) ) {
					$query->set( 'posts_per_page', 15 );
				}
				if ( is_tax( 'publication_type' ) ) {
					$query->set( 'posts_per_page', -1 );
				}
				/* make sure innovation pages display all posts */
				if ( $query->is_post_type_archive( 'innovation' ) ) {
					$query->set( 'posts_per_page', -1 );
					$query->set( 'orderby', 'menu_order' );
					$query->set( 'order', 'ASC' );
				}
				/* only show top level profile pages */
				if ( $query->is_post_type_archive( 'profile' ) ) {
					$query->set( 'orderby', 'menu_order' );
					$query->set( 'order', 'ASC' );
					$query->set( 'post_parent', 0 );
				}
			}
		}

		/**
		 * Generate dropdowns which can be used to filter custom post types in the admin area.
		 * Hooked into restrict_manage_posts.
		 */
		public function restrict_by_term() {
			global $typenow;
			switch ( $typenow ) {
				case 'project':
					$taxonomy = 'project_type';
					break;
				case 'literature':
					$taxonomy = 'publication_type';
					break;
				default:
					$taxonomy = false;
					break;
			}
			if ( $taxonomy ) {
				$selected      = isset( $_GET[ $taxonomy ] ) ? sanitize_text_field( wp_unslash( $_GET[ $taxonomy ] ) ) : '';
				$info_taxonomy = get_taxonomy( $taxonomy );
				wp_dropdown_categories( array(
					'show_option_all' => 'Show All ' . $info_taxonomy->label,
					'taxonomy'        => $taxonomy,
					'name'            => $taxonomy,
					'orderby'         => 'name',
					'selected'        => $selected,
					'show_count'      => true,
					'hide_empty'      => true,
				) );
			}
		}

		/**
		 * Converts term ID to slug whenthe taxonomy filter is used.
		 * Hooked into parse_query.
		 *
		 * @param WP_Query $query - instance of WP_Query passed by reference.
		 */
		public function convert_id_to_term_in_query( $query ) {
			global $pagenow;
			$q_vars = $query->query_vars;
			if ( 'edit.php' === $pagenow && isset( $q_vars['post_type'] ) ) {
				switch ( $q_vars['post_type'] ) {
					case 'project':
						$taxonomy = 'project_type';
						break;
					case 'literature':
						$taxonomy = 'publication_type';
						break;
					default:
						$taxonomy = false;
						break;
				}
				if ( $taxonomy ) {
					if ( isset( $q_vars[ $taxonomy ] ) && is_numeric( $q_vars[ $taxonomy ] ) && 0 !== $q_vars[ $taxonomy ] ) {
						$term                = get_term_by( 'id', $q_vars[ $taxonomy ], $taxonomy );
						$q_vars[ $taxonomy ] = $term->slug;
					}
				}
			}
		}

		/**
		 * Redefine columns in admin list table for project post type.
		 * Hooked into manage_edit-project_columns.
		 *
		 * @param array $columns - existing columns for this post type.
		 * @return array $columns - new columns for this post type.
		 */
		public function project_columns( $columns ) {
			$columns = array(
				'cb'             => '<input type="checkbox" />',
				'title'          => 'Project',
				'project_type'   => 'Project Types',
				'project_year'   => 'Year',
				'project_number' => 'Project number',
				'date'           => 'Date',
			);
			return $columns;
		}

		/**
		 * Add data to rows for custom columns.
		 * Hooked into manage_project_posts_custom_column.
		 *
		 * @param string  $column - identifier for column.
		 * @param integer $post_id - ID of post for this row.
		 */
		public function manage_project_columns( $column, $post_id ) {
			global $post;
			switch ( $column ) {
				case 'project_year':
				case 'project_number':
					$val = get_field( $column, $post_id );
					$val = empty( $val ) ? '-' : trim( $val );
					echo esc_html( $val );
					break;
				case 'project_type':
					$terms = get_the_terms( $post_id, 'project_type' );
					if ( ! empty( $terms ) ) {
						$out = array();
						foreach ( $terms as $term ) {
							$out[] = sprintf( '<a href="%s">%s</a>',
								esc_url( add_query_arg( array(
									'post_type' => $post->post_type,
									'genre'     => $term->slug,
								), 'edit.php' ) ),
								esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'genre', 'display' ) )
							);
						}
						echo join( ', ', $out );
					}
					break;
				default:
					break;
			}
		}

		/**
		 * Make project post type sortable by year and number.
		 * Hooked into manage_edit-project_sortable_columns.
		 *
		 * @param array $columns - array of sortable column identifiers.
		 * @return array $columns - modified array of sortable column identifiers.
		 */
		public function project_sortable_columns( $columns ) {
			$columns['project_year']   = 'project_year';
			$columns['project_number'] = 'project_number';
			return $columns;
		}

		/**
		 * Implement sorting on custom values for project post type.
		 * Hooked into pre_get_posts
		 *
		 * @param WP_Query $query - instance of WP_Query passed by reference.
		 */
		public function projects_admin_orderby( $query ) {
			if ( ! is_admin() ) {
				return;
			}
			$orderby = $query->get( 'orderby' );
			if ( 'project_year' === $orderby || 'project_number' === $orderby ) {
				$query->set( 'meta_key', $orderby );
				$query->set( 'orderby', 'meta_value_num' );
			}
		}

		/**
		 * Orders projects by project_date followed by menu_order in front end.
		 * Hooked int pre_get_posts.
		 *
		 * @param WP_Query $query - instance of WP_Query passed by reference.
		 */
		public function order_projects_by_date( $query ) {
			global $typenow;
			if ( ! is_admin() && isset( $query->query_vars['post_type'] ) && 'project' === $query->query_vars['post_type'] ) {
				$query->set( 'meta_key', 'project_year' );
				$query->set( 'orderby', array(
					'meta_value_num' => 'DESC',
					'menu_order'     => 'ASC',
				) );
			}
		}

		/**
		 * Register post types for Projects, Literature, Innovations and Company Profile
		 */
		public function register_post_types() {
			$labels = array(
				'name'               => 'Projects',
				'singular_name'      => 'Project',
				'menu_name'          => 'Projects',
				'all_items'          => 'All Projects',
				'add_new'            => 'Add New Project',
				'add_new_item'       => 'Add New Project',
				'edit_item'          => 'Edit Project',
				'new_item'           => 'New Project',
				'view_item'          => 'View Project',
				'search_items'       => 'Search Projects',
				'not_found'          => 'No Projects found',
				'not_found_in_trash' => 'No Projects found in Trash',
			);

			$args = array(
				'label'               => 'Projects',
				'labels'              => $labels,
				'description'         => '',
				'public'              => true,
				'show_ui'             => true,
				'show_in_rest'        => true,
				'rest_base'           => 'projects',
				'has_archive'         => true,
				'show_in_menu'        => true,
				'exclude_from_search' => false,
				'capability_type'     => 'page',
				'map_meta_cap'        => true,
				'hierarchical'        => true,
				'rewrite'             => array(
					'slug'       => 'projects',
					'with_front' => false,
				),
				'query_var'           => true,
				'menu_position'       => 6,
				'menu_icon'           => 'dashicons-star-filled',
				'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
			);
			register_post_type( 'project', $args );

			$labels = array(
				'name'               => 'Literature',
				'singular_name'      => 'Publication',
				'menu_name'          => 'Literature',
				'all_items'          => 'All Publications',
				'add_new'            => 'Add New Publication',
				'add_new_item'       => 'Add New Publication',
				'edit_item'          => 'Edit Publication',
				'new_item'           => 'New Publication',
				'view_item'          => 'View Publication',
				'search_items'       => 'Search Publications',
				'not_found'          => 'No Publications found',
				'not_found_in_trash' => 'No Publications found in Trash',
			);

			$args = array(
				'label'               => 'Literatiure',
				'labels'              => $labels,
				'description'         => '',
				'public'              => true,
				'show_ui'             => true,
				'show_in_rest'        => true,
				'rest_base'           => 'literature',
				'has_archive'         => true,
				'show_in_menu'        => true,
				'exclude_from_search' => false,
				'capability_type'     => 'page',
				'map_meta_cap'        => true,
				'hierarchical'        => true,
				'rewrite'             => array(
					'slug'       => 'literature',
					'with_front' => false,
				),
				'query_var'           => true,
				'menu_position'       => 8,
				'menu_icon'           => 'dashicons-book-alt',
				'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
			);
			register_post_type( 'literature', $args );

			$labels = array(
				'name'               => 'Innovations',
				'singular_name'      => 'Innovation',
				'menu_name'          => 'Innovation',
				'all_items'          => 'All Innovation Pages',
				'add_new'            => 'Add New Page',
				'add_new_item'       => 'Add New Innovation Page',
				'edit'               => 'Edit',
				'edit_item'          => 'Edit Innovation Page',
				'new_item'           => 'New Innovation Page',
				'view'               => 'View',
				'view_item'          => 'View Innovation Page',
				'search_items'       => 'Search Innovation Pages',
				'not_found'          => 'No Innovation Pages found',
				'not_found_in_trash' => 'No Innovation Pages found in trash',
			);

			$args = array(
				'label'               => 'Innovations',
				'labels'              => $labels,
				'description'         => '',
				'public'              => true,
				'show_ui'             => true,
				'show_in_rest'        => true,
				'rest_base'           => 'innovation',
				'has_archive'         => true,
				'show_in_menu'        => true,
				'exclude_from_search' => false,
				'capability_type'     => 'page',
				'map_meta_cap'        => true,
				'hierarchical'        => true,
				'rewrite'             => array(
					'slug'       => 'innovation',
					'with_front' => false,
				),
				'query_var'           => true,
				'menu_position'       => 7,
				'menu_icon'           => 'dashicons-lightbulb',
				'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'author', 'page-attributes' ),
			);
			register_post_type( 'innovation', $args );

			$labels = array(
				'name'                  => 'Profile pages',
				'singular_name'         => 'Profile page',
				'menu_name'             => 'Company Profile',
				'all_items'             => 'All Pages',
				'add_new'               => 'Add New',
				'add_new_item'          => 'Add New Page',
				'edit_item'             => 'Edit Page',
				'new_item'              => 'New Page',
				'view_item'             => 'View Page',
				'search_items'          => 'Search Page',
				'not_found'             => 'No Page found',
				'not_found_in_trash'    => 'No Pages found',
				'parent'                => 'Parent Page',
				'featured_image'        => 'Featured image',
				'set_featured_image'    => 'Set featured image',
				'remove_featured_image' => 'Remove featured image',
				'use_featured_image'    => 'Use featured image',
				'archives'              => 'Archives',
				'insert_into_item'      => 'Insert into page',
				'uploaded_to_this_item' => 'Uploaded to this page',
				'filter_items_list'     => 'Filter pages list',
				'items_list_navigation' => 'Page list navigation',
				'items_list'            => 'Pages list',
			);

			$args = array(
				'label'               => 'Profile pages',
				'labels'              => $labels,
				'description'         => '',
				'public'              => true,
				'show_ui'             => true,
				'show_in_rest'        => true,
				'rest_base'           => 'profile',
				'has_archive'         => true,
				'show_in_menu'        => true,
				'exclude_from_search' => false,
				'capability_type'     => 'page',
				'map_meta_cap'        => true,
				'hierarchical'        => true,
				'rewrite'             => array(
					'slug'       => 'profile',
					'with_front' => false,
				),
				'query_var'           => true,
				'menu_position'       => 5,
				'menu_icon'           => 'dashicons-star-empty',
				'supports'            => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' ),
			);
			register_post_type( 'profile', $args );
		}

		/**
		 * Registers taxonomies for Projects (project types) and Literature (publication categories)
		 */
		public function register_taxonomies() {
			$labels = array(
				'name'              => 'Project Types',
				'singular_name'     => 'Project Type',
				'menu_name'         => 'Project Types',
				'all_items'         => 'All Project Types',
				'edit_item'         => 'Edit Project Type',
				'view_item'         => 'View Project Type',
				'update_item'       => 'Update Project Type',
				'add_new_item'      => 'Add new Project Type',
				'new_item_name'     => 'New Project Type name',
				'parent_item'       => 'Parent Project Type',
				'parent_item_colon' => 'Parent Project Type:',
				'search_items'      => 'Search Project Types',
				'not_found'         => 'No Project Types found',
			);

			$args = array(
				'label'              => 'Project Types',
				'labels'             => $labels,
				'public'             => true,
				'hierarchical'       => true,
				'label'              => 'Project Types',
				'show_ui'            => true,
				'query_var'          => true,
				'rewrite'            => array(
					'slug'       => 'projects/type',
					'with_front' => false,
				),
				'show_admin_column'  => true,
				'show_in_rest'       => false,
				'rest_base'          => '',
				'show_in_quick_edit' => true,
			);
			register_taxonomy( 'project_type', array( 'project' ), $args );

			$labels = array(
				'name'                => 'Publication Categories',
				'singular_name'       => 'Publication Category',
				'menu_name'           => 'Publication Categories',
				'all_items'           => 'All Publication Categories',
				'edit_item'           => 'Edit Publication Category',
				'view_item'           => 'View Publication Category',
				'update_item'         => 'Update Publication Category',
				'add_new_item'        => 'New Publication Category',
				'new_item_name'       => 'New Category Name',
				'parent_item'         => 'Parent Publication Category',
				'parent_item_colon'   => 'Parent Publication Category:',
				'search_items'        => 'Search Publication Categories',
				'add_or_remove_items' => 'Add or remove Publication Categories',
			);

			$args = array(
				'label'              => 'Publication Categories',
				'labels'             => $labels,
				'public'             => true,
				'hierarchical'       => true,
				'label'              => 'Publication Categories',
				'show_ui'            => true,
				'query_var'          => true,
				'rewrite'            => array(
					'slug'       => 'literature/type',
					'with_front' => false,
				),
				'show_admin_column'  => true,
				'show_in_rest'       => false,
				'rest_base'          => '',
				'show_in_quick_edit' => true,
			);
			register_taxonomy( 'publication_type', array( 'literature' ), $args );
		}
	}
	new IRA_Post_Types();
}
