<?php
/**
 * Navigation for the theme
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Navigation' ) ) {
	/**
	 * Class to define sidebars and navigation menus for the theme.
	 */
	class IRA_Navigation {
		/**
		 * Constructor - registers menus and filters.
		 */
		public function __construct() {
			register_nav_menus( array(
				'main'       => 'Main Navigation',
				'profile'    => 'Profile menu',
				'innovation' => 'Innovations menu',
				'literature' => 'Literature menu',
			) );
			/* tidy up menu generation */
			add_filter( 'nav_menu_css_class', array( $this, 'nav_menu_css_class' ), 10, 2 );
			add_filter( 'nav_menu_item_id', '__return_null' );
			add_filter( 'wp_nav_menu_args', array( $this, 'nav_menu_args' ) );
		}

		/**
		 * Remove the id="" on nav menu items
		 * Return 'menu-slug' for nav menu classes
		 *
		 * @param array   $classes - Array of the CSS classes that are applied to the menu item's <li> element.
		 * @param WP_Post $item - The current menu item.
		 */
		public function nav_menu_css_class( $classes, $item ) {
			$slug      = sanitize_title( $item->title );
			$classes   = preg_replace( '/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes );
			$classes   = preg_replace( '/^((menu|page)[-_\w+]+)+/', '', $classes );
			$classes[] = 'menu-' . $slug;
			$classes   = array_unique( $classes );
			return array_filter( $classes, array( $this, 'is_element_empty' ) );
		}

		/**
		 * Clean up wp_nav_menu_args
		 * Remove the container
		 * Use p2_Nav_Walker() by default
		 *
		 * @param array $args - nav_menu arguments.
		 */
		public function nav_menu_args( $args ) {
			$nav_menu_args['container'] = false;
			if ( ! $args['items_wrap'] ) {
				$nav_menu_args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
			}
			if ( 'main' === $args['theme_location'] || 'footer' === $args['theme_location'] ) {
				$nav_menu_args['depth']  = 1;
				$nav_menu_args['walker'] = new IRA_Main_Walker();
			}
			return array_merge( $args, $nav_menu_args );
		}

		/**
		 * Callback for array_filter to identify empty elements
		 *
		 * @param string $element - element to check if empty.
		 */
		public function is_element_empty( $element ) {
			$element = trim( $element );
			return empty( $element ) ? false : true;
		}
	}
	new IRA_Navigation();
}

/**
 * Cleaner walker for wp_nav_menu() used on main navigation
 */
class IRA_Main_Walker extends Walker_Nav_Menu {
	/**
	 * Start of list before elements are added.
	 *
	 * @param string   $output Used to append additional content (passed by reference).
	 * @param int      $depth  Depth of menu item. Used for padding.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function start_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '<ul>';
	}

	/**
	 * Adds search to end of menu
	 *
	 * @param string   $output Used to append additional content (passed by reference).
	 * @param int      $depth  Depth of menu item. Used for padding.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= '<li><a class="link-search JS-search-open" href="#">Search</a></li></ul>';
	}

	/**
	 * Removes all classes from menu items
	 *
	 * @param string   $output Used to append additional content (passed by reference).
	 * @param WP_Post  $item   Menu item data object.
	 * @param int      $depth  Depth of menu item. Used for padding.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 * @param int      $id     Current item ID.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$item_html = '';
		parent::start_el( $item_html, $item, $depth, $args );
		$item_html = preg_replace( '/ class="[^"]*"/', '', $item_html );
		$output   .= $item_html;
	}

}
