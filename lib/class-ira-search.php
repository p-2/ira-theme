<?php
/**
 * Search functions
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Search' ) ) {
	/**
	 * Class to modify search parameters and function.
	 */
	class IRA_Search {
		/**
		 * Constructor - add hooks to WordPress API.
		 */
		public function __construct() {
			add_filter( 'pre_get_posts', array( $this, 'search_filter' ) );
		}

		/**
		 * Adds other post types to search query.
		 *
		 * @param WP_Query $query - query object passed to pre_get_posts filter.
		 */
		public function search_filter( $query ) {
			if ( $query->is_search && ! is_admin() ) {
				// make sure site searches encompass all post types
				$query->set( 'post_type', array( 'post', 'project', 'profile', 'literature' ) );
			}
			return $query;
		}
	}
	new IRA_Search();
}
