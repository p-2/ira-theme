<?php
// this may take a while...
set_time_limit(0);

// need this for external data table queries
global $wpdb;

$acf_field_keys = array(
	'organisation_name' => 'field_576e525ced0ba',
    'street_address' => 'field_576e5290ed0bb',
    'locality' => 'field_576e529ded0bc',
    'postcode' => 'field_576e52aaed0bd',
    'fax' => 'field_5777e643e6b09',
    'telephone' => 'field_576e52b8ed0be',
    'contact_email' => 'field_576e52d2ed0bf',
    'block' => 'field_577138bad30a1',
    'slides' => 'field_56ea68b569f23',
    'related_content' => 'field_5754721028416',
    'additional_media' => 'field_56ec48dae759e',
    'additional_media|type' => 'field_56ec49b4e75a1',
    'additional_media|title' => 'field_56ec496ee75a0',
    'additional_media|url' => 'field_56ec491be759f',
    'additional_media|file' => 'field_56ec4b2386237',
    'project_year' => 'field_56ea6a3e89c01',
    'project_number' => 'field_56ea6ad189c02',
    'awards' => 'field_5742a8cff8de7',
    'awards|name' => 'field_5742a8fff8de8',
    'awards|year' => 'field_5742a92df8de9',
    'awards|awarding_body' => 'field_5742aaf2f8dec',
    'awards|url' => 'field_5742ab3cf8ded',
    'awards|award_type' => 'field_5742ac0bf8dee',
    'bibliography' => 'field_5742a9d9f8dea',
    'bibliography|publication_type' => 'field_5742af35f8df4',
    'bibliography|title' => 'field_5742aa44f8deb',
    'bibliography|publication_month' => 'field_5755da2611826',
    'bibliography|publication_year' => 'field_5755d9dc11825',
    'bibliography|publication_url' => 'field_577579dfe8650',
    'bibliography|author_name' => 'field_5742ae7cf8df0',
    'bibliography|publisher' => 'field_5742aea1f8df1',
    'bibliography|isbn' => 'field_5742af15f8df3',
    'bibliography|isbn' => 'field_5742af15f8df3',
    'external_links' => 'field_574754b287908',
    'external_links|url' => 'field_574754ec8790a',
    'external_links|title' => 'field_574755058790b',
    'start_type' => 'field_5755ebe72b080',
    'start_zoom' => 'field_5755ec9c2b081',
    'locations' => 'field_574697938d2fb',
    'locations|title' => 'field_574697a58d2fc',
    'locations|description' => 'field_574697b68d2fd',
    'locations|image' => 'field_5755fb41eca95',
    'locations|location' => 'field_574697de8d2fe',
    'option_google_analytics_key' => 'field_5755ff28a5ae1',
    'option_google_webmaster_tools_key' => 'field_5755ff3da5ae2',
    'option_bing_webmaster_tools_key' => 'field_5755ff4da5ae3',
    'option_google_api_key' => 'field_5755ffc8a5ae4'
);

//need to structure data here for import into repeater fields
$repeaters = array();

// stuff products information in here
$product_info = array();

// store post titles
$project_titles = array();

// get all pages 	        
$pages = get_posts( array(
	"numberposts" => -1,
	"post_type" => array("project","innovation","literature","product"),
	"post_parent" => 0,
	"post_status" => "any",
	"nopaging"    => true
) );

// get all links from link manager
$links = get_bookmarks();

// convert project metadata to ACF field data 
foreach ($pages as $page ) {

	// ATTACHMENTS

	// convert uploaded image files to ACF galleries
	$images = get_posts( array(
		'post_type' => 'attachment',
		'post_mime_type'=> 'image',
		'numberposts' => -1,
		'nopaging' => true,
		'post_status' => null,
		'post_parent' => $page->ID,
		'orderby' => 'menu_order'
	) );
	if ( count( $images ) ) {
		$slides = array();
		foreach ( $images as $img ) {
			$slides[] = (string) $img->ID;
		}
		if ( $page->post_type === 'product' ) {
			// add images to woocommerce product gallery
			add_post_meta( 
				$page->ID,
				'_product_image_gallery', 
				implode(',', $slides), 
				true) 
			or update_post_meta( 
				$page->ID, 
				'_product_image_gallery', 
				implode(',', $slides) 
			);
		} else {
			// add data
			add_post_meta( 
				$page->ID,
				'slides', 
				$slides, 
				true) 
			or update_post_meta( 
				$page->ID, 
				'slides', 
				$slides 
			);
			// add key
			add_post_meta( 
				$page->ID,
				'_slides', 
				$acf_field_keys['slides'], 
				true) 
			or update_post_meta( 
				$page->ID, 
				'_slides', 
				$acf_field_keys['slides']
			);
		}
	}

	// convert uploaded media to ACF "Additional Media"
	$additional_media_files = get_posts( array(
		'post_type' => 'attachment',
		'post_mime_type'=> array(
			'application/pdf',
			'text/plain',
			'audio'
		),
		'numberposts' => -1,
		'nopaging' => true,
		'post_status' => null,
		'post_parent' => $page->ID,
		'orderby' => 'menu_order'
	) );
	if ( count( $additional_media_files ) ) {
		if ( ! isset( $repeaters["additional_media_" . $page->ID] ) ) {
			$repeaters["additional_media_" . $page->ID] = array(
				"post_id" => $page->ID,
				"meta_key" => "additional_media",
				"items" => array()
			);
		}
		for ( $i = 0; $i < count( $additional_media_files ); $i++ ) {
			// add data for each field
			switch ( $additional_media_files[$i]->post_mime_type ) {
				case 'application/pdf':
				case 'text/plain':
					// file upload of some sort
					$repeaters["additional_media_" . $page->ID]["items"][] = array(
						"type" => "file",
						"url" => "",
						"file" => $additional_media_files[$i]->ID,
						"title" => $additional_media_files[$i]->post_title
					);
					break;
				default:
					// only other mime type is audio
					$repeaters["additional_media_" . $page->ID]["items"][] = array(
						"type" => "audiofile",
						"url" => "",
						"file" => $additional_media_files[$i]->ID,
						"title" => $additional_media_files[$i]->post_title
					);
					break;
			}
		}
	}

	// now get additional media from postmeta
	$ira_media = get_post_meta($page->ID, 'ira_media', true);
	if ( $ira_media && is_array( $ira_media ) ) {
		foreach ( $ira_media as $iram ) {
			if ( ! isset( $repeaters["additional_media_" . $page->ID] ) ) {
				$repeaters["additional_media_" . $page->ID] = array(
					"post_id" => $page->ID,
					"meta_key" => "additional_media",
					"items" => array()
				);
			}
			$repeaters["additional_media_" . $page->ID]["items"][] = array(
				"type" => ( strpos( $iram["url"], 'soundcloud' ) !== false ? 'audio': 'video' ),
				"url" => $iram["url"],
				"file" => "",
				"title" => $iram["title"]
			);
		}
	}

	// get data from paypal plugin to use in woocommerce
	if ( $page->post_type === 'product' ) {
		$product_info[$page->ID] = get_post_meta($page->ID, "paypal", true);
		$product_info[$page->ID]["post_content"] = $page->post_content;
	}
	// get post titles for use in maps
	if ( $page->post_type === 'project' ) {
		$project_titles[$page->ID] = $page->post_title;
	}
}

//print_r($product_info);exit;

// convert data from data plugin to ACF fields
$rows = $wpdb->get_results( "SELECT * FROM `" . $wpdb->prefix . "iradata`;" );
if ( count( $rows ) ) {
	foreach ( $rows as $row ) {
		$data = unserialize( $row->data );
		if ( is_array( $data["thread_project"] ) && count( $data["thread_project"] ) ) {
			$project_ids = array();
			if ( ! is_array( $data["thread_project"][0] ) ) {
				$project_ids = array($data["thread_project"][0]);
			} else {
				$project_ids = $data["thread_project"][0];
			}
			foreach ( $project_ids as $post_id ) {
				switch ( $row->context ) {
					case "awards":
						if ( ! isset( $repeaters["awards_" . $post_id] ) ) {
							$repeaters["awards_" . $post_id] = array(
								"post_id" => $post_id,
								"meta_key" => "awards",
								"items" => array()
							);
						}
						$repeaters["awards_" . $post_id]["items"][] = array(
							"name" => $row->title,
							"year"  => $row->date,
							"awarding_body" => $data["awarding_body"],
							"url" => ( isset( $data["awarding_body_url"] ) ? $data["awarding_body_url"]: ''),
							"award_type" => $data["award_type"]
						);
						break;
					case "literature":
						if ( ! isset( $repeaters["bibliography_" . $post_id] ) ) {
							$repeaters["bibliography_" . $post_id] = array(
								"post_id" => $post_id,
								"meta_key" => "bibliography",
								"items" => array()
							);
						}
						$url = ( isset( $data['url'] ) ) ? $data['url']: '';
						$repeaters["bibliography_" . $post_id]["items"][] = array(
							"title" => $row->title,
							"author_name" => $data["author"],
							"publication_year" => $data["date_year"],
							"publication_month" => $data["date_month"],
							"publisher" => $data["publisher"],
							"isbn" => $data["isbn"],
							"publication_type" => $data["pubtype"],
							"publication_url" => $url
						);
				}
			}
		}
	}
}

// convert maps to ACF fields
$mapdata = $wpdb->get_results( "SELECT * FROM $wpdb->postmeta WHERE `meta_key` LIKE 'maps4posts%'" );
if ( count( $mapdata ) ) {
	foreach ( $mapdata as $pmap ) {
		$data = unserialize( $pmap->meta_value );
		switch ( $pmap->meta_key ) {
			case "maps4posts_data":
				foreach ( array( 'start_type', 'start_zoom' ) as $key ) {
					$val = ($key === 'start_type')? strtoupper( $data[$key] ): intval( $data[$key] );
					// add data
					add_post_meta( 
						$pmap->post_id, 
						$key,
						$val,
						true
					) or update_post_meta( 
						$pmap->post_id,
						$key,
						$val
					);
					// add key
					add_post_meta( 
						$page->ID,
						'_' . $key, 
						$acf_field_keys[$key], 
						true) 
					or update_post_meta( 
						$page->ID, 
						'_' . $key, 
						$acf_field_keys[$key]
					);
				}
				break;
			case "maps4posts_markers":
				if ( ! isset( $repeaters["markers_" . $pmap->post_id] ) ) {
					$repeaters["markers_" . $pmap->post_id] = array(
						"post_id" => $pmap->post_id,
						"meta_key" => "locations",
						"items" => array()
					);
				}
				$title = $data["title"];
				if ( $title === "" && isset( $project_titles[$pmap->post_id] ) ) {
					$title = $project_titles[$pmap->post_id];
				}
				$repeaters["markers_" . $pmap->post_id]["items"][] = array(
					"title" => $title,
					"description" => "",
					"image" => "", 
					"location" => array(
						"lat" => $data["lat"],
						"lng" => $data["lng"],
						"address" => ""
					)
				);
				break;
		}
	}
}

// add options for keys
$key_options = array(
	'option_google_analytics_key' => '',
    'option_google_webmaster_tools_key' => '',
    'option_bing_webmaster_tools_key' => '',
    'option_google_api_key' => 'AIzaSyA94YjLxR5Fv18dzxqHtYuQmhUXI_AOk60'
);
foreach ( $key_options as $option_key => $option_value ) {
	update_option( $option_key, $option_value );
	update_option( '_' . $option_key, $acf_field_keys[$option_key] );
}

// update options for media
$media_options = array(
	'thumbnail_size_w' => 235,
	'thumbnail_size_h' => 235,
	'thumbnail_crop' => 1,
	'large_size_w' => 1200,
	'large_size_h' => 1200
);
foreach ( $media_options as $option_key => $option_value ) {
	update_option( $option_key, $option_value );
}

// add new options for address
$address = array(
	'organisation_name' => 'Ian Richie Architects',
	'street_address' => '110 Three Colt Street',
	'locality' => 'London',
	'postcode' => 'E14 8AZ',
	'telephone' => '+44 (0)20 7338 1100',
	'fax' => '+44 (0) 20 7338 1199',
	'contact_email' => 'mail@ianritchiearchitects.co.uk'
);
foreach ( $address as $option_key => $option_value ) {
	update_option( 'options_' . $option_key, $option_value );
	update_option( '_options_' . $option_key, $acf_field_keys[$option_key] );
}

// populate woocommerce fields
foreach ( $product_info as $post_id => $data ) {
	$map = array();
	$map['_sku'] = $data["code"];
	$map['_price'] = $data["price"];
	$map['_regular_price'] = $data["price"];
	$map['_stock_status'] = 'outofstock';
	$map['_manage_stock'] = 'yes';
	$map['_stock'] = 0;
	foreach ( $map as $key => $val ) {
		add_post_meta( 
			$post_id, 
			$key,
			$val,
			true
		) or update_post_meta( 
			$post_id,
			$key,
			$val
		);
	}
	// append description to product content
	$content = $data["post_content"] . $data["description"];
	wp_update_post( array(
		'ID' => $post_id,
		'post_content' => $content
	) );

}

// convert related items and links to ACF fields
$bookmarks = get_bookmarks();
$threads = $wpdb->get_results( "SELECT pm.meta_value, pm.post_id, p.post_type FROM $wpdb->postmeta pm, $wpdb->posts p WHERE pm.post_id = p.ID AND pm.meta_key = 'thread_meta' AND pm.meta_value != 'a:0:{}'" );

if ( count( $threads ) ) {

	// add ACF post object links to meta
	$threadmeta = array();
	foreach( $threads as $thread ) {
		$linked_ids = unserialize( $thread->meta_value );

		foreach ( $linked_ids as $post_id ) {
			if ( strpos( $post_id, 'link' ) !== false ) {
				if ( ! isset( $repeaters["external_links_" . $thread->post_id] ) ) {
					$repeaters["external_links_" . $thread->post_id] = array(
						"post_id" => $thread->post_id,
						"meta_key" => 'external_links',
						"items" => array()
					);
				}

				foreach ( $bookmarks as $bookmark ) {
					if ( $post_id == 'link_' . $bookmark->link_id ) {
						$repeaters["external_links_" . $thread->post_id]["items"][] = array(
							"title" => $bookmark->link_name,
							"url" => $bookmark->link_url
						);
					}
				}
			} else {
				if ( ! isset( $threadmeta['thread_' . $thread->post_id] ) ) {
					$threadmeta['thread_' . $thread->post_id] = array(
						"post_id" => $thread->post_id,
						"threads" => array()
					);
				}
				$threadmeta['thread_' . $thread->post_id]["threads"][] = $post_id;
			}
		}
	}
	// add to post meta
	foreach ( $threadmeta as $tmdata ) {
		add_post_meta( 
			$tmdata["post_id"], 
			'related_content',
			$tmdata["threads"],
			true
		) or update_post_meta( 
			$tmdata["post_id"],
			'related_content',
			$tmdata["threads"]
		);
		// add key
		add_post_meta( 
			$tmdata["post_id"],
			'_related_content', 
			$acf_field_keys['related_content'], 
			true) 
		or update_post_meta( 
			$tmdata["post_id"], 
			'_related_content', 
			$acf_field_keys['related_content']
		);

	}
}

// add ACF repeater field data
foreach ( $repeaters as $r ) {
	if ( count( $r["items"] ) ) {
		add_post_meta(
			$r["post_id"], 
			$r['meta_key'],
			count( $r["items"] ),
			true
		) or update_post_meta( 
			$r["post_id"], 
			$r['meta_key'],
			count( $r["items"] )
		);
		// add key
		add_post_meta( 
			$r["post_id"],
			'_' . $r['meta_key'], 
			$acf_field_keys[$r['meta_key']], 
			true) 
		or update_post_meta( 
			$r["post_id"], 
			'_' . $r['meta_key'], 
			$acf_field_keys[$r['meta_key']]
		);
		for ( $i = 0; $i < count( $r["items"] ); $i++ ) {
			foreach ( $r["items"][$i] as $key => $val ) {
				add_post_meta( 
					$r["post_id"], 
					$r['meta_key'] . '_' . $i . '_' . $key,
					$val,
					true
				) or update_post_meta( 
					$r["post_id"],
					$r['meta_key'] . '_' . $i . '_' . $key,
					$val
				);
				add_post_meta( 
					$r["post_id"], 
					'_' . $r['meta_key'] . '_' . $i . '_' . $key,
					$acf_field_keys[$r['meta_key'] . '|' . $key],
					true
				) or update_post_meta( 
					$r["post_id"],
					'_' . $r['meta_key'] . '_' . $i . '_' . $key,
					$acf_field_keys[$r['meta_key'] . '|' . $key]
				);
			}
		}
	}
}

// rename product category
$wpdb->query("UPDATE $wpdb->term_taxonomy SET `taxonomy` = 'product_cat' WHERE `taxonomy` = 'product_type'");

// add navigation menus
$items = array(
	'News' => home_url( '/news/' ),
	'About' => home_url( '/profile/' ),
	'Literature' => home_url( '/literature/' ),
	'Innovation' => home_url( '/innovation/' ),
	'Projects' => home_url( '/projects/' ),
	'Contact' => home_url( '/contact/' )
);
$menus = array(
	array(
		'theme_location' => 'main',
		'label' => 'Main menu',
		'items' => $items
	),
	array(
		'theme_location' => 'profile',
		'label' => 'Profile menu',
		'items' => array()
	),
	array(
		'theme_location' => 'footer',
		'label' => 'Footer menu',
		'items' => $items
	)
);
$locations = array();
foreach ( $menus as $menu ) {
	$menu_exists = wp_get_nav_menu_object( $menu['label'] );
	if ( ! $menu_exists ) {
		$menu_id = wp_create_nav_menu( $menu['label'] );
		$locations[$menu['theme_location']] = $menu_id;
		foreach ( $menu['items'] as $label => $data ) {
			wp_update_nav_menu_item( $menu_id, 0, array(
				'menu-item-title' =>  $label,
				'menu-item-url' => $data, 
				'menu-item-status' => 'publish'
			) );
		}
	}
}
if ( count( $locations ) ) {
	set_theme_mod('nav_menu_locations', $locations);
}

// remove all data fom database
$to_delete = array(
	'meta' => array(
		'maps4posts_data',
		'maps4posts_markers',
		'paypal',
		'thread_meta',
		'project_number',
		'project_year',
		'publication_details',
		'ira_media'
	),
	'tables' => array(
		'wp_iradata'
	),
	'options' => array(
		'paypal_options',
		'maps4posts_options'
	)
);

// make sure we have access to dbDelta
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

foreach ( $to_delete as $type => $keys ) {
	switch ( $type ) {
		case 'meta':
			$wpdb->query( sprintf( "DELETE FROM $wpdb->postmeta WHERE `meta_key` IN('%s');", implode("','", $keys ) ) );
			break;
		case 'options':
			$wpdb->query( sprintf( "DELETE FROM $wpdb->options WHERE `option_name` IN('%s');", implode("','", $keys ) ) );
			break;
		case 'tables':
			foreach ( $keys as $tablename ) {
				$sql = "DROP TABLE IF EXISTS $tablename;";
				dbDelta($sql);
			}
			break;
	}
}

