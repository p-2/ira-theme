<?php
/**
 * Class to define ACF fields for the theme
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_ACF' ) ) {
	/**
	 * Class used to define and register hooks for the ACF plugin
	 */
	class IRA_ACF {
		/**
		 * Constructor - registers hooks and filters
		 */
		public function __construct() {
			// Add options page.
			add_action( 'acf/init', array( $this, 'add_options_page' ) );
			// Filters title on Homepage blocks flexible layout items.
			add_filter( 'acf/fields/flexible_content/layout_title/name=block', array( $this, 'change_homepage_block_title' ), 10, 4 );
			// Filter to add API key.
			add_filter( 'acf/fields/google_map/api', array( $this, 'add_api_key' ) );
		}

		/**
		 * Adds an ACF controlled theme options page
		 */
		public function add_options_page() {
			acf_add_options_page();
		}

		/**
		 * Filters homepage block titles to include the titls field so they are easier to sort
		 *
		 * @param string $title - Layout Label.
		 * @param array  $field - the flexible content field settings array.
		 * @param array  $layout - the current layout settings array.
		 * @param int    $i - the current layout index (the first layout index is 0).
		 */
		public function change_homepage_block_title( $title, $field, $layout, $i ) {
			// Load block title sub field.
			$text = get_sub_field( 'block_title' );
			if ( $text ) {
				// Append to title in italics.
				$title .= ' <em>' . $text . '</em>';
			}
			// Return (maybe) modified title.
			return $title;
		}

		/**
		 * Filter to add API key for Google Maps
		 *
		 * @param array $api - array of settings used by the Google Maps widget.
		 */
		public function add_api_key( $api ) {
			$api_key = get_field( 'google_api_key', 'option' );
			if ( $api_key ) {
				$api['key'] = $api_key;
			}
			return $api;
		}
	}
	new IRA_ACF();
}
