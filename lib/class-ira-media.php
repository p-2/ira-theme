<?php
/**
 * Media functions
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Media' ) ) {
	/**
	 * Class to define media related settings
	 */
	class IRA_Media {
		/**
		 * Constructor - media functions and filter registrtion.
		 */
		public function __construct() {
			/* This theme uses post thumbnails. */
			add_theme_support( 'post-thumbnails' );

			/* Size of post thumbnails. */
			set_post_thumbnail_size( 460, 200, true );

			/* Add image side for homepage sliders (happens to be the same as one of the woocommerce sizes). */
			add_image_size( 'medium_large', 768, 0, false );

			/* Stop caption from being included with images by default */
			add_filter( 'disable_captions', '__return_true' );

			/* When linking to images, ensure the large image is linked to (not the original) */
			add_filter( 'wp_get_attachment_link', array( $this, 'get_attachment_link_filter' ), 10, 4 );
		}

		/**
		 * Filter hooked to wp_get_attachment_link to return the URL of the "large" image
		 * in place of the media URL (original image).
		 *
		 * @param string  $content - content to be filtered.
		 * @param integer $post_id - ID of current post.
		 * @param string  $size - size of image being embedded.
		 * @param boolean $permalink - Whether to add permalink to image.
		 */
		public function get_attachment_link_filter( $content, $post_id, $size, $permalink ) {
			// Only do this if we're getting the file URL.
			if ( ! $permalink ) {
				// This returns an array of (url, width, height).
				$image = wp_get_attachment_image_src( $post_id, 'large' );
				$new_content = preg_replace( '/href=\'(.*?)\'/', 'href=\'' . $image[0] . '\'', $content );
				return $new_content;
			} else {
				return $content;
			}
		}
	}
	new IRA_Media();
}
