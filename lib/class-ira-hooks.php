<?php
/**
 * Filters for use in theme
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Hooks' ) ) {
	/**
	 * Class to provide actions and filters for content output
	 */
	class IRA_Hooks {
		/**
		 * Constructor - adds filters using WordPress API
		 */
		public function __construct() {
			// Filter to standardise video URLs in iframe embeds.
			add_filter( 'ira_video_embed_url', array( $this, 'ira_video_embed_url' ) );
		}

		/**
		 * Parses URLs for vimeo and youtube and standardises output
		 * for use in an iframe embed
		 *
		 * @param string $url - URL of video to standardise.
		 */
		public function ira_video_embed_url( $url ) {
			if ( preg_match( '/(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/', $url, $matches ) ) {
				return sprintf( 'https://player.vimeo.com/video/%d?title=0&byline=0&portrait=0', $matches[5] );
			} elseif ( preg_match( '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $matches ) ) {
				return sprintf( 'https://www.youtube.com/embed/%s', $matches[1] );
			} else {
				return $url;
			}
		}
	}
	new IRA_Hooks();
}


