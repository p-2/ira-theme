<?php
/**
 * Theme administration functions
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Admin' ) ) {
	/**
	 * Class to handle upgrades and other theme admin tasks.
	 */
	class IRA_Admin {
		/**
		 * Theme version
		 *
		 * @var string $version - semver formatted version of theme.
		 */
		public static $version = '2.1.16';

		/**
		 * Constructor - registers with WordPress API
		 */
		public function __construct() {
			add_action( 'init', array( $this, 'upgrade' ), 2 );
		}

		/**
		 * Theme upgrade
		 */
		public function upgrade() {
			$current_version = get_option( 'ira_theme_version' );
			if ( $current_version !== self::$version ) {
				switch ( $current_version ) {
					case '2.1.1':
						// put projects in order by year.
						global $wpdb;
						$args         = array(
							'numberposts' => -1,
							'post_type'   => 'project',
							'post_parent' => 0,
							'meta_key'    => 'project_year',
							'orderby'     => array(
								'meta_value_num' => 'DESC',
								'menu_order'     => 'ASC',
							),
							'post_status' => 'any',
						);
						$all_projects = get_posts( $args );
						$num_projects = count( $all_projects );
						for ( $i = 0; $i < $num_projects; $i++ ) {
							$wpdb->update(
								$wpdb->posts,
								array(
									'menu_order' => $i,
								),
								array( '%d' ),
								array(
									'ID' => $all_projects[ $i ]->ID,
								),
								array( '%d' )
							);
						}
						break;

				}
				/* update the version option */
				update_option( 'ira_theme_version', self::$version );
			}
		}
	}
	new IRA_Admin();
}
