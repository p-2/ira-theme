<?php
/**
 * Ian Ritchie Architects WordPress Theme scripts
 *
 * @package IRA_Theme
 */

if ( ! class_exists( 'IRA_Scripts' ) ) {
	/**
	 * Class to enqueue scripts and styles for front-end and admin.
	 */
	class IRA_Scripts {
		/**
		 * Constructor - adds hooks to WordPress API.
		 */
		public function __construct() {
			/* add theme scripts to front end */
			add_action( 'wp_enqueue_scripts', array( $this, 'theme_scripts' ) );

			/* add admin scripts to back end */
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );

			/* swap jquery for google CDN version */
			add_action( 'wp_enqueue_scripts', array( $this, 'jquery_cdn' ) );

			/* add a filter to script loader for jQuery local fallback */
			add_filter( 'script_loader_src', array( $this, 'jquery_local_fallback' ), 10, 2 );

			/* add a filter to script loader toremove type attribute */
			add_filter( 'script_loader_tag', array( $this, 'clean_script_tag' ) );

			/* Script to add js class. */
			add_action( 'wp_head', array( $this, 'add_html_js_class' ), 1 );

			/* Ajax response for maps query. */
			add_action( 'wp_ajax_loadmarkers', array( $this, 'get_project_markers' ) );
		}

		/**
		 * Enqueue theme scripts
		 */
		public function theme_scripts() {
			/* register IRA site script */
			wp_register_script(
				'ira',
				get_template_directory_uri() . '/js/scripts.min.js',
				array( 'jquery' ),
				IRA_Admin::$version,
				true
			);
			wp_localize_script(
				'ira',
				'iratheme',
				array(
					'dir'            => get_template_directory_uri(),
					'google_api_key' => get_field( 'google_api_key', 'option' ),
					'ajax_url'       => admin_url( 'admin-ajax.php' ),
				)
			);
			wp_enqueue_script( 'ira' );
			wp_enqueue_style(
				'ira-style',
				get_template_directory_uri() . '/css/style.min.css',
				array(),
				IRA_Admin::$version
			);
		}

		/**
		 * Enqueue admin scripts
		 */
		public function admin_scripts() {
			/* register admin script */
			wp_register_script(
				'ira-admin',
				get_template_directory_uri() . '/js/admin.js',
				array( 'jquery' ),
				IRA_Admin::$version,
				true
			);
			wp_enqueue_script( 'ira-admin' );
		}

		/**
		 * Get jquery from google CDN
		 */
		public function jquery_cdn() {
			if ( ! is_admin() ) {
				/* re-register jquery with the CDN google hosted version */
				wp_deregister_script( 'jquery' );
				wp_register_script(
					'jquery',
					'//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js',
					array(),
					'3.0.0',
					true
				);
			}
		}

		/**
		 * Variable used to track whether the jQuery fallback has been used
		 *
		 * @var boolean $add_fallback - whether jQuery fallback has been employed.
		 */
		private static $add_fallback = false;

		/**
		 * Add a local fallback to include jQuery
		 *
		 * @param string $src - script source URL.
		 * @param string $handle - WordPress handle assigned to script in loeader.
		 */
		public function jquery_local_fallback( $src, $handle ) {
			if ( self::$add_fallback ) {
				echo '<script>window.jQuery || document.write(\'<script src="' . get_template_directory_uri() . '/js/vendor/jquery.min.js"><\/script>\')</script>' . "\n";
				self::$add_fallback = false;
			}
			if ( 'jquery' === $handle ) {
				self::$add_fallback = true;
			}
			return $src;
		}

		/**
		 * Adds a js class to the html element
		 */
		public function add_html_js_class() {
			print( '<script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>' );
		}

		/**
		 * Removes the type attribute from enqueued script tags
		 *
		 * @param string $input - script tag passed to filter.
		 */
		public function clean_script_tag( $input ) {
			$input = str_replace( "type='text/javascript' ", '', $input );
			$input = str_replace( 'type="text/javascript" ', '', $input );
			return $input;
		}

		/**
		 * Gets all markers for projects
		 */
		public function get_project_markers() {
			$markersjson = get_transient( 'ira_markers' );
			if ( false === $markersjson ) {
				$projects = get_posts( array(
					'numberposts' => -1,
					'post_type'   => 'project',
					'post_status' => 'publish',
					'nopaging'    => true,
				) );
				$markers  = array();
				foreach ( $projects as $project ) {
					if ( 105 !== $project->ID ) {
						$project_url = get_permalink( $project->ID );
						if ( have_rows( 'locations', $project->ID ) ) {
							while ( have_rows( 'locations', $project->ID ) ) {
								the_row();
								$location  = get_sub_field( 'location' );
								$html      = sprintf( '<div class="ira-infowindow"><h3>%s</h3>', get_sub_field( 'title' ) );
								$image_url = get_sub_field( 'image' );
								if ( $image_url ) {
									$html .= sprintf( '<img src="%s" style="width:100%%;height:auto;" alt="%s">', $image_url, esc_attr( get_sub_field( 'title' ) ) );
								}
								if ( '' !== trim( get_sub_field( 'description' ) ) ) {
									$html .= wpautop( wptexturize( get_sub_field( 'description' ) ) );
								}
								$html     .= sprintf( '<p><a href="https://www.google.com/maps/dir/Current+Location/%s,%s" target="google_maps">get directions</a></p></div>', $location['lat'], $location['lng'] );
								$markers[] = array(
									'lat'   => $location['lat'],
									'lng'   => $location['lng'],
									'title' => get_sub_field( 'title' ),
									'html'  => $html,
								);
							}
						}
					}
				}
				$markersjson = wp_json_encode( $markers );
				set_transient( 'ira_markers', $markers, YEAR_IN_SECONDS );
			}
			print( $markersjson );
			exit;
		}
	}
	new IRA_Scripts();
}
