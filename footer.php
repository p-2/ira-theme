<?php
get_template_part('templates/util/search', 'modal');
get_template_part('templates/util/bibliography', 'modal');
get_template_part('templates/util/map', 'project');
get_template_part('templates/util/map', 'footer');
?>
<footer>
<div class="container">
   
	<address class="vcard">
		<?php
		print('<p>');
		$org = get_field( 'organisation_name', 'options' );
		if ( $org ) {
			printf('<span class="org fn">%s</span><br>', $org );
		}
		if ( have_rows( 'locations', 'option' ) ) {
			print('<a class="show-map" href="#footer-map">');
		}
		foreach ( array('street-address', 'locality', 'postal-code') as $adr_field ) {
			$line = get_field( $adr_field, 'options' );
			if ( $line ) {
				printf('<span class="%s">%s</span> ', $adr_field, $line );
			}
			if ($adr_field === 'street-address' ) {
				print('<br>');
			}
		}
		if ( have_rows( 'locations', 'option' ) ) {
			print('</a>');
		}
		print('</p>');
        $tel = get_field( 'telephone', 'options' );
        $email = get_field( 'contact_email', 'options' );
        if ( $tel || $email ) {
        	print('<p>Contact Us<br>');
	        if ( $tel ) {
	        	printf('<a class="tel" href="tel:%s">%s</a><br>', preg_replace('/[^\+0-9]/', '', $tel), $tel );
	        }
	        if ( $email ) {
	        	printf('<a class="email" href="mailto:%s">%s</a>', $email, $email);
	        }
	        print('</p>');
	    }
        ?>
    </address>

</div>
</footer>
<!-- Footer ends-->
<?php
get_template_part('templates/util/google', 'analytics');
wp_footer();
?>
</body>
</html>