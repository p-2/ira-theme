<!-- Homepage slider-->
<div class="wrapper">
    <div class="container homepage-section homepage-featured">
        <div class="homepage-section--tag homepage-featured--latest-tag">Featured</div>

        <div class="JS-home-slider">

            <div class="slidecontainer">
                <div class="homepage-featured--item">
                    <h2 class="homepage-section--title homepage-featured--item--title">
                        <a href="#">Sainsbury Wellcome Centre</a>
                    </h2>

                    <div class="homepage-featured--item--image" style="background-image: url(https://placehold.it/600x400?text=Project+image);"></div>

                    <div class="homepage-section--description homepage-featured--item--description">
                        <p>Following an international competition, Ian Ritchie Architects were appointed in September 2009 as architects and design team leaders to design a new research centre for Neural Circuits and Behaviour at University College London. This project at UCL is being funded by The Gatsby Charitable Foundation and The Wellcome Trust. Arup are structural and services engineers.</p>
                        <p class="homepage-section--link homepage-featured--item--link"><a href="#">View this project</a></p>
                        <p class="homepage-section--link homepage-featured--item--link"><a href="#">View all projects</a></p>
                    </div>
                </div>
            </div>

            <div class="slidecontainer">
                <div class="homepage-featured--item">
                    <h2 class="homepage-section--title homepage-featured--item--title">
                        <a href="#">Another project</a>
                    </h2>

                    <div class="homepage-featured--item--image" style="background-image: url(https://placehold.it/600x400?text=Project+image);"></div>

                    <div class="homepage-section--description homepage-featured--item--description">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam consequatur, dolores harum impedit iure magni odio placeat, praesentium provident quaerat quisquam sed vel vitae voluptas voluptatibus? Accusantium ducimus magnam odit.</p>
                        <p class="homepage-section--link homepage-featured--item--link"><a href="#">View this project</a></p>
                        <p class="homepage-section--link homepage-featured--item--link"><a href="#">View all projects</a></p>
                    </div>
                </div>
            </div>


        </div>

    </div>
</div>

<!-- Homepage slider ends-->
<!-- Homepage About -->

<div class="wrapper wrapper--dark">
    <div class="container homepage-section homepage-about">
        <div class="homepage-section--tag homepage-about--tag">The Practice</div>
        <div class="homepage-section--title homepage-about--title">About Us</div>
        <div class="homepage-about--image" style="background-image: url(https://placehold.it/600x400?text=About+image);"></div>
        <div class="homepage-section--description homepage-about--description">
            <p>In 1981, Ian Ritchie established his own practice, Ian Ritchie Architects, on the basis of a possible commission for a private house from Ursula Colahan. Since 1978, Ian Ritchie had been working part-time as a consultant at Arup within Peter Rice’s Lightweight Structures Group, and also with Peter Rice on the Shelterspan system. He was also a partner in Chrysalis Architects and teaching at the AA with Mike Davies and Alan Stanton.</p>
            <p class="homepage-section--link homepage-about--link"><a href="#">More about us</a></p>

            <address class="vcard homepage-about--address">
                <div class="adr">
                    <div class="org">Ian Ritchie Architects</div>
                    <div class="street-address">110 Three Colt Street</div>
                    <div class="locality">London</div>
                    <div class="postal-code">E14 8AZ</div>
                    <a class="map" href="#">View map</a>
                </div>

                <div class="tel">+44 (0)20 7338 11008</div>
                <a class="email" href="mailto:mail@ianritchiearchitects.co.uk">mail@ianritchiearchitects.co.uk</a>
            </address>
        </div>
    </div>

</div>

<!-- Homepage About ends -->
<!-- Literature -->
<div class="wrapper">
    <div class="container homepage-section homepage-literature">
        <div class="homepage-section--tag homepage-literature--tag">Literature</div>
        <h2 class="homepage-section--title homepage-literature--title">Being: An Architect</h2>
        <div class="homepage-literature--image" style="background-image: url(https://placehold.it/600x400?text=Literature+image);"></div>
        <div class="homepage-section--description homepage-literature--description">
            <p>Following an international competition, Ian Ritchie Architects were appointed in September 2009 as architects and design team leaders to design a new research centre for Neural Circuits and Behaviour at University College London. This project at UCL is being funded by The Gatsby Charitable Foundation and The Wellcome Trust. Arup are structural and services engineers.</p>
            <p class="homepage-section--link homepage-literature--link"><a href="#">View all publications</a></p>
        </div>
    </div>
</div>
<!-- Literature ends -->
<!-- Philosophy -->
<div class="wrapper wrapper--dark">
    <div class="container homepage-section homepage-philisophy">
        <div class="homepage-section--tag homepage-philosophy--tag">Our Philosophy</div>
        <div class="homepage-philosophy--list JS-home-philo-slider">
            <div class="homepage-philosophy--item">
                <p>Our conceptual thinking integrates art, science, technology and economy with architecture and landscape.</p>
            </div>

            <div class="homepage-philosophy--item">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur, assumenda autem consequuntur delectus.</p>
            </div>

            <div class="homepage-philosophy--item">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aspernatur, assumenda autem consequuntur delectus.</p>
            </div>
        </div>
        <p class="homepage-section--link homepage-philisophy--link"><a href="#">Read our philosophy</a></p>
    </div>
</div>

<!-- Philosophy ends -->

<!-- Homepage news -->
<div class="wrapper">
    <div class="container homepage-section homepage-news">
        <div class="homepage-section--tag homepage-news--latest-tag">latest News</div>

        <div class="JS-home-slider">

            <div class="slidecontainer">
                <div class="homepage-news--item">
                    <h2 class="homepage-section--title homepage-news--item--title">
                        <a href="#">El Jicarito School in collaboration with Seeds of Learning</a>
                    </h2>

                    <div class="homepage-news--item--image" style="background-image: url(https://placehold.it/600x400?text=News+image);"></div>

                    <div class="homepage-section--description homepage-news--item--description">
                        <p>Tania has been collaborating with the NGO ‘Seeds of Learning’ to develop a design for an innovative and sustainable school for 27 children in the rural community of ‘El Jicarito’ (Nicaragua), and now they need help to make this happen. A crowd-funding campaign has been set up for your to help them reach their goal:
                            <a href="https://www.indiegogo.com/projects/el-jicarito-school#/">https://www.indiegogo.com/projects/el-jicarito-school#/</a></p>
                        <p class="homepage-section--link homepage-news--item--link"><a href="#">Continue reading</a></p>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Homepage news ends-->