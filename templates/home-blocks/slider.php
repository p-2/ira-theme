<!-- Homepage slider-->
<div class="wrapper wrapper--<?php the_sub_field('theme');?>">
    <div class="container homepage-section homepage-slider">
        <div class="homepage-section--tag homepage-slider--latest-tag"><?php the_sub_field('block_title'); ?></div>

            <?php

            if ( have_rows('items')) {

                echo '<div class="JS-slider">';
                while ( have_rows('items') ) : the_row();
                    ?>

                    <div class="slidecontainer">
                        <div class="homepage-slider--item">
                            <h2 class="homepage-section--title homepage-slider--item--title">
                                <a href="<?php the_sub_field('item_link') ?>"><?php the_sub_field('item_title'); ?></a>
                            </h2>
                            <?php
                            $image_url = false;
							$image_id = get_sub_field('item_image');
							if ( $image_id ) {
								$image_details = wp_get_attachment_image_src( $image_id, 'large' );
								if ( $image_details ) {
									$image_url = $image_details[0];
								}
							}
							if ($image_url ) {
								printf( '<div class="homepage-slider--item--image" style="background-image: url(%s)"></div>', $image_url );
							}
							?>
                            <div class="homepage-section--description homepage-slider--item--description">

                                <?php
                                the_sub_field('item_content');

                                if ( get_sub_field('item_link_text') ) {
                                	$link_type = get_sub_field('item_link_type');
                                	$link_url = ( $link_type === 'pagelink' ) ? get_sub_field('item_link'): get_sub_field('item_external_link');
                                	if ( $link_url !== '' ) {
                                    ?>

                                    <p class="homepage-section--link homepage-slider--item--link"><a href="<?php echo $link_url; ?>"><?php the_sub_field('item_link_text') ?></a></p>

                                    <?php
                                	}
                                }

                                if ( get_sub_field('secondary_item_link_text') ) {
                                	$link_type = get_sub_field('secondary_link_type');
                                	$link_url = ( $link_type === 'pagelink' ) ? get_sub_field('secondary_item_link'): get_sub_field('secondary_external_link');
                                	if ( $link_url !== '' ) {
                                    ?>

                                    <p class="homepage-section--link homepage-slider--item--link"><a href="<?php echo $link_url; ?>"><?php the_sub_field('secondary_item_link_text') ?></a></p>

                                    <?php
                                   	}
                                }

                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                endwhile;
                echo '</div>';
            }

            ?>

    </div>
</div>

<!-- Homepage slider ends-->