<!-- Text Carousel -->
<div class="wrapper wrapper--<?php the_sub_field('theme'); ?>">
    <div class="container homepage-section homepage-text-carousel">
        <div class="homepage-section--tag homepage-text-carousel--tag"><?php the_sub_field('block_title'); ?></div>
        <?php

            if ( have_rows('text_carousel_content')) {
                echo '<div class="homepage-text-carousel--list JS-home-text-carousel">';
                while ( have_rows('text_carousel_content') ) : the_row();
                    echo '<div class="homepage-text-carousel--item">';
                    echo '<p>' . get_sub_field('text_carousel_text') .'</p>';

                    echo '</div>';

                endwhile;
                echo '</div>';
            }

        // Append link if specified

        if (get_sub_field('text_carousel_link')) {
            ?>

            <p class="homepage-section--link homepage-text-carousel--link"><a href="<?php the_sub_field('text_carousel_link') ?>"><?php the_sub_field('text_carousel_link_text') ?></a></p>

            <?php
        }

        ?>
    </div>
</div>

<!-- Text Carousel ends -->