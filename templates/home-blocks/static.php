<?php
$class = get_sub_field('theme');
if ( empty( $class ) ) {
	$class = 'dark';
}
$class .= ' ';
$class .= get_sub_field('image_align');


$image_url = false;
$image_id = get_sub_field('item_image');
if ( $image_id ) {
	$image_details = wp_get_attachment_image_src( $image_id, 'medium' );
	if ( $image_details ) {
		$image_url = $image_details[0];
	}
}

$link_type = get_sub_field('link_type');
$link_url = ( $link_type === 'pagelink' ) ? get_sub_field('item_link'): get_sub_field('item_external_link');

?>
<!-- Homepage Static block -->

<div class="wrapper wrapper--<?php echo trim($class); ?>">
    <div class="container homepage-section homepage-static">
        <div class="homepage-section--tag homepage-static--tag"><?php the_sub_field('block_title'); ?></div>
        <?php
        $itemtitle = get_sub_field('item_title');
        printf( '<div class="homepage-section--title homepage-static--title">%s</div>', $itemtitle );
        
        if ($image_url ) {
        	if ( $link_url !== '' ) {
        		printf( '<div class="homepage-static--image" style="background-image: url(%s)"><a href="%s"></a></div>', $image_url, $link_url );
        	} else {
        		printf( '<div class="homepage-static--image" style="background-image: url(%s)"></div>', $image_url );
        	}
        } 
        ?>
        <div class="homepage-section--description homepage-static--description">
            <?php
            the_sub_field('item_content');

            if ( get_sub_field('item_link_text') ) {
            	if ( $link_url !== '' ) {
                ?>

                <p class="homepage-section--link homepage-static--link"><a href="<?php echo $link_url; ?>"><?php the_sub_field('item_link_text') ?></a></p>

                <?php
            	}
            }

            if ( get_sub_field('secondary_item_link_text') ) {
            	$link_type = get_sub_field('secondary_link_type');
            	$link_url = ( $link_type === 'pagelink' ) ? get_sub_field('secondary_item_link'): get_sub_field('secondary_external_link');
            	if ( $link_url !== '' ) {
                ?>

                <p class="homepage-section--link homepage-static--link"><a href="<?php echo $link_url; ?>"><?php the_sub_field('secondary_item_link_text') ?></a></p>

                <?php
               	}
            }

            ?>
        </div>
    </div>
</div>

<!-- Homepage About ends -->