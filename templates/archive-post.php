<?php
/**
 * template for post archive
 * acts as the default for all post types
 */

print('<div class="container main">');

$post_type = get_post_type();

if ( ! have_posts() ) {

    get_template_part( 'templates/util/404' );

} else {

	get_template_part( 'templates/nav/archive-subnav', $post_type );

	print('<div class="content page">');
	$pagetitle = get_field($post_type, 'option');
	if ( ! $pagetitle ) {
		$obj = get_post_type_object( $post_type );
		$pagetitle = $obj->labels->name;
	}
	if ( is_year() ) {
        $pagetitle .= ' - ' . get_the_date( 'Y' );
	}
	printf('<h1 class="page-title">%s</h1>', $pagetitle );
	printf('<div class="posts-list list">', $post_type );


	while ( have_posts() ) : the_post();

		printf('<div class="list-item %s-list-item">', $post_type );

		if ( has_post_thumbnail( $post->ID ) ) {
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "thumbnail" );
			if ( false !== $thumb ) {
				printf('<div class="list-image %s-list-image" style="background-image: url(%s);"></div>', $post_type, $thumb[0] );
			}
		}

        printf('<div class="list-details %s-list-details">', $post_type );
        printf('<div class="list-title %s-list-title"><a href="%s">%s</a></div>', $post_type, get_permalink( $post->ID ), get_the_title() );
        printf('<div class="list-excerpt">%s</div>', get_the_excerpt() );
        printf('<div class="post-date">%s</div>', get_the_date('F Y') );
        printf('<a class="list-link %s-list-link" href="%s">%s</a>', $post_type, get_permalink( $post->ID ), get_the_title() );
        print('</div>');
    	
		print('</div>');
	endwhile;
	print('</div></div>');

}
print('</div>');
