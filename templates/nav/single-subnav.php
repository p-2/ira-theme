<?php
$post_type = get_post_type();

if ( has_nav_menu( $post_type ) ) {
	$menu = wp_nav_menu( array( 
        'theme_location' => $post_type,
        'container'      => '',
        'items_wrap'     => '%3$s',
        'echo'           => false,
        'fallback_cb'    => false
    ) );
    $menu = str_replace( array('<li', '</li>'), array('<dd', '</dd>'), $menu );
    $obj = get_post_type_object( $post_type );
	?>
    <nav class="subnav">
        <button class="subnav-toggle JS-subnav-toggle">
            In this section
        </button>
        <div class="subnav-items JS-subnav">
            <!--<dl class="projects-filters--list hide-less-than-desttop">
                <dt class="projects-filters--type">View</dt>
                <dd class="projects-filters--item selected"><a href="#">Grid</a></dd>
                <dd class="projects-filters--item"><a href="#">List</a></dd>
            </dl>-->

            <dl class="subnav-list">
                <dt>In this section</dt>
                <?php print($menu); ?>
            </dl>
        </div>
    </nav>
<?php
}