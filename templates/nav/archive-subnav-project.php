<?php
$uri = home_url() . $_SERVER["REQUEST_URI"];
$qspos = strpos($uri, '?');
if ( $qspos !== false ) {
	$uri = substr($uri, 0, $qspos );
}
$project_types = get_categories( array(
	"taxonomy" => "project_type"
) );
$selected = false;
$selected_name = '';
if ( is_tax('project_type') ) {
	$term = get_queried_object();
	$selected = $term->term_id;
	$selected_name = $term->name;
}
?>
    <nav class="subnav">
        <button class="subnav-toggle JS-subnav-toggle">
            Type
        </button>
        <?php if ( $selected_name ) {
        	//printf('<span class="term--selected">Showing: %s</span>', $selected_name);
        }
        ?>
        <div class="subnav-items JS-subnav">

            <dl class="subnav-list sort hide-less-than-desttop">
                <dt>Sort by</dt>
                <dd class="active desc"><a href="#" class="sortby-date">Date</a></dd>
                <dd><a href="#" class="sortby-atoz">A-Z</a></dd>
            </dl>

            <dl class="subnav-list layout hide-less-than-desttop">
                <dt>View</dt>
                <dd class="active"><a href="#" class="change-view togrid">Grid</a></dd>
                <dd><a href="#" class="change-view tolist">List</a></dd>
            </dl>

            <dl class="subnav-list filters">
                <dt>Type</dt>
                <dd class="<?php if ( ! $selected ) echo ' active'; ?>"><a href="<?php echo site_url(); ?>/projects/" class="show-all">All</a></dd>
                <?php
                foreach ( $project_types as $type ) {
                	$sel = ( $type->term_id === $selected ) ? ' class="active"': '';
                	printf('<dd%s><a href="%s/projects/type/%s/" data-type-slug="%s" class="filter">%s</a></dd>', $sel, site_url(), $type->slug, $type->slug, $type->name );
                }
                ?>
            </dl>
        </div>
    </nav>