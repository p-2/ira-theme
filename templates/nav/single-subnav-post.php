<?php
$postyear = false;
$archives_html = wp_get_archives( array(
	'type' => 'yearly',
	'echo' => 0,
	'format' => 'custom',
	'before' => '<dd>',
	'after' => '</dd>'
) );
?>
    <nav class="subnav">
        <button class="subnav-toggle JS-subnav-toggle">
            Archives
        </button>
        <div class="subnav-items JS-subnav">
            <!--<dl class="projects-filters--list hide-less-than-desttop">
                <dt class="projects-filters--type">View</dt>
                <dd class="projects-filters--item selected"><a href="#">Grid</a></dd>
                <dd class="projects-filters--item"><a href="#">List</a></dd>
            </dl>-->

            <dl class="subnav-list">
                <dt>Archives</dt>
                <?php print($archives_html); ?>
            </dl>
        </div>
    </nav>
