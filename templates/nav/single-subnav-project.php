	<nav class="subnav project-info">
		<button class="subnav-toggle JS-subnav-toggle">
			Information
		</button>
		<div class="subnav-items subnav-projects JS-subnav">
			<dl class="subnav-list">
				<dt>Information</dt>
				<?php
				if ( get_field('project_year') ) {
					echo '<dd>' . get_field('project_year') . '</dd>';
				}
				if ( get_field('project_location') ) {
					if ( have_rows('locations') ) {
						echo '<dd class="show-if-js"><a href="#project-map" class="show-map">' . get_field('project_location') . '</a></dd>';
					} else {
						echo '<dd>' . get_field('project_location') . '</dd>';
					}
				} else {
					if ( have_rows('locations') ) {
						echo '<dd class="show-if-js"><a href="#project-map" class="show-map">Map</a></dd>';
					}
				}
				if ( have_rows('bibliography') ) {
					echo '<dd class="show-if-js"><a href="#" data-featherlight="#bibliography">Publications</a></dd>';
				}
				?>
			</dl>

			<?php

			// Project Awards loop
			if ( have_rows('awards') ):

				$awardsHtml = '<dl class="subnav-list"><dt>Awards</dt>';
				$awards = array();
				$count = 0;
				// loop through the rows of data
				while ( have_rows('awards') ) : the_row();

					// text for award
					$awardtxt = get_sub_field('name');
					// get the year
					$year = get_sub_field('year');
					$yeartxt = $year ? " (" . $year . ")": '';

					$awardsHtml .= sprintf('<dd><span class="awarding-body">%s</span>%s</dd>', $awardtxt, $yeartxt );

				endwhile;

				$awardsHtml .= '</dl>';

				echo $awardsHtml;

			endif;

			// Additional Media
			$additionalHtml = '';
			$additional = array();
			if ( have_rows('additional_media') ) {
				$additional_media = array(
					'Video' => array(),
					'Audio' => array(),
					'Downloads' => array(),
					'Links' => array()
				);
				while ( have_rows('additional_media') ) : the_row();
					$field_type = get_sub_field('type');
					switch( $field_type ) {
						case 'video':
							$additional_media['Video'][] = sprintf( '<dd><a href="%s" class="modal modal-video">%s</a></dd>', apply_filters( 'ira_video_embed_url', get_sub_field('url') ), get_sub_field('title') );
							break;
						case 'audio':
							$additional_media['Audio'][] = sprintf( '<dd><a href="%s" class="modal modal-audio">%s</a></dd>', get_sub_field('url'), get_sub_field('title') );
							break;
						case 'audiofile':
							$additional_media['Audio'][] = sprintf( '<dd><a href="%s" class="modal modal-audio">%s</a></dd>', get_sub_field('file'), get_sub_field('title') );
							break;
						case 'file':
							$additional_media['Downloads'][] = sprintf( '<dd><a href="%s" class="file-download">%s</a></dd>', get_sub_field('file'), get_sub_field('title') );
							break;
						case 'link':
							$additional_media['Links'][] = sprintf( '<dd><a href="%s" class="external-link">%s</a></dd>', get_sub_field('url'), get_sub_field('title') );
							break;
					}
				endwhile;
				foreach ( $additional_media as $label => $items ) {
					if ( count( $items ) ) {
						$additionalHtml .= sprintf( '<dl class="subnav-list"><dt>%s</dt>%s</dl>', $label, implode("", $items) );
					}
				}
				echo $additionalHtml;

			}

			// Related Content
			$related_posts = get_field('related_content');
			$relatedHtml = '';
			$related = array();

			if ( $related_posts ) {

				// sort into post types
				foreach( $related_posts as $related_post) {
					if ( $related_post->post_status == "publish" ) {
						if ( ! isset( $related[$related_post->post_type] ) ) {
							$related[$related_post->post_type] = array();
						}
						$related[$related_post->post_type][] = $related_post;
					}
				}

				// headings (in display order)
				$related_headings = array(
					'project' => 'Related projects',
					'innovation' => 'Innovation',
					'post' => 'In the news',
					'literature' => 'Literature'
					/*'product' => 'Shop'*/
				);

				foreach ( $related_headings as $post_type => $title ) {
					if ( isset( $related[$post_type] ) ){
						$relatedHtml .= sprintf( '<dl class="subnav-list"><dt>%s</dt>', $title );
						foreach ( $related[$post_type] as $related_post ) {
							$relatedHtml .= sprintf( '<dd><a href="%s">%s</a></dd>', get_permalink($related_post->ID), get_the_title($related_post->ID) );
						}
						$relatedHtml .= '</dl>';
					}
				} 
				echo $relatedHtml;

			}

			?>
		</div>
	</nav>
