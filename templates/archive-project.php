<?php
/**
 * template for project archive
 * displays date and location for each item
 */

print('<div class="container main">');

$post_type = get_post_type();

$pagetitle = get_field($post_type, 'option');
if ( ! $pagetitle ) {
	$obj = get_post_type_object( $post_type );
	$pagetitle = $obj->labels->name;
}
if (is_tax()) {
	$term = get_queried_object();
	$pagetitle = $term->name;
}

if ( ! have_posts() ) {

    get_template_part( 'templates/util/404' );

} else {

	get_template_part( 'templates/nav/archive-subnav', $post_type );

	print('<div class="content page">');
	printf('<h1 class="page-title">%s</h1>', $pagetitle );
	printf('<div class="posts-list %s-list grid">', $post_type );

	while ( have_posts() ) : the_post();

		$classes = array('list-item');

		// add a class for each project type
    	$types = get_the_terms( $post->ID, "project_type");
    	if ( is_array( $types ) && count( $types ) ) {
    		foreach ($types as $type) {
    			$classes[] = "type-" . $type->slug;
    		}
    	}

    	// get alphabetical  sort key
		$sortkey_atoz = preg_replace('/[^a-z0-9]/', '', strtolower( remove_accents( $post->post_title ) ) );

		// get date sortkey
		if ( get_field('project_year') ) {
			$sortkey_date = get_field('project_year');
		} else {
			$sortkey_date = get_the_date( 'Y', $post->ID );
		}
		$sortkey_date .= sprintf('%05d', (99999 - intval($post->menu_order)));

		printf('<div class="%s" data-sortkey_atoz="%s" data-sortkey_date="%s">', implode(" ", $classes), $sortkey_atoz, $sortkey_date );

		if ( has_post_thumbnail( $post->ID ) ) {
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "post-thumbnail" );
			if ( false !== $thumb ) {
				printf('<div class="list-image %s-list-image" style="background-image: url(%s);"></div>', $post_type, $thumb[0] );
			}
		}

        printf('<div class="list-details %s-list-details">', $post_type );
        if ( get_field('project_year') ) {
        	//printf('<div class="project-year">%s</div>', get_field('project_year') );
        }
        printf('<div class="list-title %s-list-title"><a href="%s">%s</a></div>', $post_type, get_permalink( $post->ID ), get_the_title() );
        if ( get_field('project_location') ) {
        	printf('<div class="project-location">%s</div>', get_field('project_location') );
        }
        printf('<a class="list-link %s-list-link" href="%s">%s</a>', $post_type, get_permalink( $post->ID ), get_the_title() );
        print('</div>');
    	
		print('</div>');
	endwhile;
	print('</div></div>');
}
print('</div>');
