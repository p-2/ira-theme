<?php
/**
 * template for single post
 * acts as the default for all post types
 */

print('<div class="container main">');

if ( ! have_posts() ) {

    get_template_part( 'templates/util/404' );

} else {

	get_template_part( 'templates/nav/single-subnav', get_post_type() );

	while (have_posts()) : the_post();

	    print('<div class="content page">');
    	printf('<h1 class="page-title">%s</h1>', get_the_title() );
    	the_content();
 		$pagelinks = wp_link_pages( array(
			'before'           => '<nav class="pagination"><ul><li>',
			'after'            => '</li></ul></nav>',
			'link_before'      => '',
			'link_after'       => '',
			'next_or_number'   => 'number',
			'separator'        => '</li><li>',
			'nextpagelink'     => 'Next page',
			'previouspagelink' => 'Previous page',
			'pagelink'         => '%',
			'echo'             => 0
		) );
		echo preg_replace( '/<li>([0-9 ]+)<\/li>/', '<li class="current">$1</li>', $pagelinks );

    	get_template_part( 'templates/util/image-gallery', get_post_type() );

		print('</div>');

	endwhile;
}

print('<div class="push"></div></div>');
