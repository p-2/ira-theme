<?php
/*
 * Search results loop
 */

print('<div class="container main">');

if ( ! have_posts() ) {

	print('<div class="content page">');
	print('<h1 class="page-title">Search Results</h1>');
	print('<p>Sorry, but your search for <strong>' . get_search_query() . '</strong> got no results, <a href="#" class="JS-search-open">Please try again</a>.<p>');
	print('</div>');

} else {

	global $wp_query;
	//print( '<pre>' . print_r($wp_query, true) . '</pre>');
	$query = get_search_query();

	// populate variables containing results details
	$found = $wp_query->found_posts;
	$page = ( $wp_query->query_vars['paged'] === 0 ) ? 1: $wp_query->query_vars['paged'];
	$per_page = $wp_query->query_vars['posts_per_page'];
	$from = ( $page === 1 ) ? 1: ( ( ( $page - 1 ) * $per_page ) + 1 );
	$to = ( ($per_page * $page ) > $found ) ? $found: ($per_page * $page );

	// search subnav
    print('<nav class="subnav"><button class="subnav-toggle JS-subnav-toggle">Results</button><div class="subnav-items JS-subnav"><dl class="subnav-list"><dt>Results</dt>');
    $pl = ( $found === 1 ) ? '': 's';
	printf('<dd>%d result%s for &ldquo;%s&rdquo;</dd>', $found, $pl, $query );
	if ( $found > 1 ) {
		printf('<dd>Showing results %d to %d.</dd>', $from, $to );
	}
	print('</dl></div></nav>');

	// start search results content
	print( '<div class="content page">' );

	get_search_form();

	print( '<div class="posts-list search-list list">' );

	while ( have_posts() ) : the_post();

		printf( '<div class="list-item search-list-item search-list-item-%s">', esc_attr( $post->post_type ) );
		$pt = ( $post->post_type === 'post' ) ? 'News': ucfirst($post->post_type);
		if ( has_post_thumbnail( $post->ID ) ) {
			$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "post-thumbnail" );
			if ( false !== $thumb ) {
				printf('<div class="list-image %s-list-image" style="background-image: url(%s);"><span>%s</span></div>', $pt, $thumb[0], $pt);
			}
		} else {
			printf('<div class="list-image %s-list-image"><span>%s</span></div>', $pt, $pt);
		}

        print('<div class="list-details">' );
        printf('<div class="list-title"><a href="%s">%s</a></div>', get_permalink( $post->ID ), get_the_title() );

        // get excerpt
        $excerpt = get_the_excerpt();
        if ( ! empty( $query) ) {
        	// highlight search terms
	        $excerpt = preg_replace( '/(' . preg_quote( $query ) . ')/i', '<span class="query_term">$1</span>', wpautop( $excerpt ) );
	    }
	    printf('<div class="list-excerpt">%s</div>', $excerpt );
        printf('<a class="list-link %s-list-link" href="%s">%s</a>', $post->post_type, get_permalink( $post->ID ), get_the_title() );
        print('</div>');
    	
		print('</div>');
	endwhile;
	print('</div></div>');

	print('<div class="pagination">');
	print( paginate_links( array(
		'base' => home_url( "/search/" ) . urlencode(get_search_query()) . '%_%',
		'format' => '/page/%#%',
		'total' => ceil( ( $found / $per_page ) ),
		'current' => $page,
		'prev_next' => false,
		'type' => 'list'
	) ) );
	print('</div>');

}
print('</div>');
