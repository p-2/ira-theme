<?php
/**
 * template for post archive
 * acts as the default for all post types
 */

print('<div class="container main">');

$post_type = get_post_type();

$pagetitle = get_field($post_type, 'option');
if ( ! $pagetitle ) {
	$obj = get_post_type_object( $post_type );
	if ( $obj ) {
		$pagetitle = $obj->labels->name;
	}
}

if ( is_tax() ) {
	$term = get_queried_object();
	$pagetitle = $term->name;
}

if ( ! have_posts() ) {

    get_template_part( 'templates/util/404' );

} else {

	get_template_part( 'templates/nav/archive-subnav', $post_type );

	print('<div class="content page">');
	printf('<h1 class="page-title">%s</h1>', $pagetitle );
	printf('<div class="posts-list %s-list grid">', $post_type );
	if ( ! is_tax() ) {
		// main archive page - show list of taxonomy terms
		$terms = get_terms(
			array(
				'taxonomy' => 'publication_type',
				'hide_empty' => true,
				'fields' => 'all_with_object_id',
			)
		);

		if ( count( $terms ) ) {
			foreach ( $terms as $term ) {
				printf('<div class="list-item %s-list-item">', $post_type );
				$term_thumbnail = get_field( 'term_image', 'publication_type_' . $term->term_id );
				if ( $term_thumbnail ) {
					$thumb = wp_get_attachment_image_src( $term_thumbnail, "post-thumbnail" );
					if ( false !== $thumb ) {
						printf('<div class="list-image %s-list-image" style="background-image: url(%s);"></div>', $post_type, $thumb[0] );
					}
				}
				printf('<div class="list-details %s-list-details">', $post_type );
				printf('<div class="list-title %s-list-title"><a href="%s">%s</a></div>', $post_type, get_term_link( $term ), $term->name );
				printf('<a class="list-link %s-list-link" href="%s">%s</a>', $post_type, get_term_link( $term ), $term->name );
				print('</div></div>');
			}
		}
	} else {
		// standard loop
		while ( have_posts() ) : the_post();

			printf('<div class="list-item %s-list-item">', $post_type );

			if ( has_post_thumbnail( $post->ID ) ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "post-thumbnail" );
				if ( false !== $thumb ) {
					printf('<div class="list-image %s-list-image" style="background-image: url(%s);"></div>', $post_type, $thumb[0] );
				}
			}

			printf('<div class="list-details %s-list-details">', $post_type );
			printf('<div class="list-title %s-list-title"><a href="%s">%s</a></div>', $post_type, get_permalink( $post->ID ), get_the_title() );
			printf('<a class="list-link %s-list-link" href="%s">%s</a>', $post_type, get_permalink( $post->ID ), get_the_title() );
			print('</div></div>');
		endwhile;
	}
	print('</div></div>');

}
print('</div>');
