<!-- Search modal -->
<div class="search-modal JS-search-container">
    <div class="search-modal--content">
    <?php get_search_form(); ?>
    </div>
    <button class="search-modal--close JS-search-close">
        <span>Close search</span>
    </button>
</div>
<!-- Search modal ends -->