<?php
if ( have_rows('bibliography') ) {
    

    // loop through the rows of data, storing each one in an array
    $publications = array();
    while ( have_rows('bibliography') ) : the_row();

    	$html = '<dd class="pub">';
    	$pubdate = '';
    	$datenum = 0;
    	if ( get_sub_field('publication_month') && get_sub_field('publication_year') ) {
    		$month = get_sub_field('publication_month');
    		if ( intVal($month) > 0 ) {
	    		$pubdate .= get_sub_field('publication_month') . '/';
    			$datenum += intVal(get_sub_field('publication_month'));
    		}
    	}
    	if ( get_sub_field('publication_year') ) {
    		$pubdate .= get_sub_field('publication_year');
    		$datenum += ( intVal(get_sub_field('publication_year')) * 100);
    	}
    	if ( $pubdate ) {
	    	$html .= sprintf('<span class="date">%s</span> ', $pubdate );
    	}
    	if ( get_sub_field('author') ) {
    		$html .= sprintf('<span class="author">%s</span>, ', get_sub_field('author') );
    	}
    	if ( get_sub_field('publication_url') ) {
    		$html .= sprintf('<a href="%s" class="external title">%s</a> ', get_sub_field('publication_url'), get_sub_field('title') );
    	} else {
    		$html .= sprintf('<span class="title">%s</span> ', get_sub_field('title') );
    	}
    	if ( get_sub_field('publisher') ) {
    		$html .= sprintf('<span class="publisher">%s</span> ', get_sub_field('publisher') );
    	}
    	if ( get_sub_field('isbn') ) {
    		$html .= sprintf('<br><span class="isbn">ISBN: %s</span>', get_sub_field('isbn') );
    	}
    	$html .= '</dd>';
    	$publications[] = array(
    		'datenum' => $datenum,
    		'title' => get_sub_field('title'),
    		'type' => get_sub_field('publication_type'),
    		'html' => $html
    	);

    endwhile;

    // sort publications by type, date and title
    $sorted_publications = array_orderby($publications, 'type', SORT_ASC, 'datenum', SORT_DESC, 'title', SORT_DESC);

	$bibliographyHtml = '<div id="bibliography" class="bibliography-modal"><div class="bibcontainer"><dl>';

    /* loop through sorted publications, listing by type */
    $currentType = '';
    $headings = array(
    	'book' => 'Books',
    	'journal' => 'Journal Articles',
    	'online' => 'Online publications'
    );
    foreach ($sorted_publications as $pub) {
    	if ( $currentType !== $pub['type'] ) {
    		$heading = isset($headings[$pub['type']])? $headings[$pub['type']]: ucfirst($pub['type']);
    		$bibliographyHtml .= sprintf('<dt>%s</dt>', $heading);
    		$currentType = $pub['type'];
    	}
    	$bibliographyHtml .= $pub['html'];
    }
    
    $bibliographyHtml .= '</dl></div></div>';

    echo $bibliographyHtml;
}
/**
 * taken from http://php.net/manual/en/function.array-multisort.php
 */
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row) {
                $tmp[$key] = $row[$field];
            }
            $args[$n] = $tmp;
        }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
}