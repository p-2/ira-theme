<?php

// Get the ACF gallery object
$images = get_field('slides');

// if the gallery object exists
if ( $images ):

    // Gallery container
    $galleryHtml = '<div class="gallery"><ul class="gallery--list">';

    foreach( $images as $image ):

    	//print('<pre>' . print_r($image, true) . '</pre>');

        $galleryHtml .= '<li class="gallery--item">';

        // Include gallery-lightbox class as hook for Featherlight Gallery functionality
        $galleryHtml .= '<a class="gallery-lightbox" href="' . $image['sizes']['large'] . '">';

        $galleryHtml .= '<img src="' . $image['sizes']['thumbnail'] . '" alt="' . esc_attr($image['alt']) . '"  data-title="' . esc_attr($image['title']) . '" data-caption="' . esc_attr(wpautop($image['caption'])) . '" data-description="' . esc_attr($image['description']) . '" />';

        $galleryHtml .= '</a>';

        $galleryHtml .= '</li>';

    endforeach;

    // Close gallery container
    $galleryHtml .= '</ul></div>';

    // Output gallery html
    echo $galleryHtml;

endif;
