<?php if( have_rows( 'locations' ) ): ?>
<div id="project-map" class="mapmodal" data-start-type="<?php echo get_field('start-type'); ?>" data-start-zoom="<?php echo get_field('start-zoom'); ?>">
<?php while ( have_rows( 'locations' ) ) : the_row(); 
	$location = get_sub_field('location');
	printf('<div class="marker" data-lat="%s" data-lng="%s" data-title="%s">', $location['lat'], $location['lng'], esc_attr( get_sub_field('title') ) );
	print('<div class="ira-infowindow">');
	$image = get_sub_field('image');
	if ( $image ) {
		printf('<img src="%s" alt="%s">', $image['sizes']['thumbnail'], esc_attr( get_sub_field('title') ) );
	}
	printf('<h2>%s</h2>', get_sub_field('title') );
	if ( trim( get_sub_field('description') ) !== '' ) {
		print wpautop( wptexturize( get_sub_field('description') ) );
	}
	printf('<p><a href="https://www.google.com/maps/dir/Current+Location/%s,%s" target="google_maps">Get directions</a></p>', $location['lat'], $location['lng'] );
	print('</div></div>');
endwhile; ?>
<div class="map" id="project-map_map"></div>
<button class="close-map-button">close</button>
</div>
<?php endif; ?>
