Ian Ritchie Architects Wordpress Theme
======================================

Changelog
---------

### 2.1

Release of theme in new design

### 2.0 

Redevelopment of the theme utilising a new design. Reliance on custom plugins removed in favour of the use of the Advanced Custom Fields plugin.

### 1.1

Redevelopment of the theme using a responsive page layout.

### 1.0

Initial release of the theme. In addition to the theme itself, the site also required 8 custom plugins in order to operate (most of which defined custom post types).