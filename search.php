<?php
/**
 * template for search results
 */
get_header();
get_template_part( 'templates/search' );
get_footer();